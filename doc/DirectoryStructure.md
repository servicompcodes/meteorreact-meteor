app/                                                      ## Directorio principal de la aplicacion
  .meteor/                                                ## Directorio del framework propiamente tal
  client/
    main.css                                              ## Estilos en ccs de la aplicacion
    main.js                                               ## Punto de entrada para ejecutar scripts del lado del cliente
    main.html                                             ## Página principal de entrada, donde se injecta React.
    theme.less                                            ## Configuraciones por default del set de componentes UI/UX Ant Design
  imports/                                                ## Directorio que no carga nada a menos que sea importado por otro archivo
    startup/                                              ## Define scripts que correran al inicio de la aplicacion
      client/
        accounts-config.js                                ## Configuracion de coleccion Users de Meteor
        index.js                                          ## Punto de entrada para ejecutar scripts del lado del cliente
        Routes.jsx                                        ## Componente de React que se encarga de manejar el acceso a las diversas rutas
      server/
        fixtures.js                                       ## Script de llenado inicial de Base de Datos con datos de ejemplo
        index.js                                          ## Punto de entrada para ejecutar scripts del lado del servidor
    ui/
      layout/                                             ## Organizacion general de los componentes visuales de la aplicacion
        contenido/                                        ## Cada colección tiene un directorio con sus componentes visualres relacionados
          Arduinos/
          Automoviles/
          Estadisticas/
          Eventos/
          Permisos/
          Tags/
          Trabajadores/
          Usuarios/
          Visitas/
          Home.jsx                                       ## Componente visual que carga la pantalla de inicio, de forma dinámica con respecto al tipo de  usuario.
          NotFound.jsx                                   ## Componente que muestra una página de error en caso que la url indroducida no sea válida
        navbarmenu/                                      ## Carpeta que almacena componentes relacionados al NavbarMenu
        Contenido.jsx                                    ## Componente principal, que carga de forma dinámica las diversas pantallas de la aplicacion
        NavbarMenu.jsx                                   ## Menú superior de la aplicacion
        Notificaciones.jsx                               ## Componente que se encarga de desplegar diversas notificaciones
        SidebarMenu.jsx                                  ## Menu lateral de la app
      App.jsx                                            ## Punto de entrada general a los componentes visuales
  lib/
    collections/                                         ## Cada dominio lógico tiene su propio script, el cual define la colección y sus métodos.
      arduinos.js
      automoviles.js
      eventos.js
      images.js
      permisos.js
      tags.js
      trabajadores.js
      usuarios.js
      visitas.js
  public/
    fonts/                                              ## Tipografías utilizadas
    images/                                             ## Imágenes públicas
  server/
    main.js                                             ## Punto de entrada para ejecutar scripts del lado del servidor
