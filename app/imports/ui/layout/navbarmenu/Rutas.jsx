import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch, Link, withRouter } from 'react-router-dom';
import { Icon, Breadcrumb, Alert } from 'antd';

@withRouter
export default class Rutas extends Component {

  constructor() {
     super();
     this.state = {
       pathname: ''
     }
   }

 static contextTypes = {
   router: React.PropTypes.object
 }

 componentWillReceiveProps(nextProps, nextContext){
   this.setState({ pathname: nextContext.router.route.location.pathname });
 }

 componentDidMount() {
   this.setState({ pathname: this.context.router.route.location.pathname });
 }

  render() {

    const pathSnippets = this.state.pathname.split('/').filter(i => i);

    const breadcrumbNameMap = {
      '/eventos': 'Eventos',
      '/automoviles': 'Automoviles',
      '/tags': 'Tags',
      '/permisos': 'Permisos',
      '/trabajadores': 'Trabajadores',
      '/visitas': 'Visitas',
      '/componentesprobados': 'Componentes Probados',
      '/arduinos': 'Arduinos',
      '/miperfil': 'Mi Perfil',
      '/usuarios': 'Usuarios',
      '/estadisticas': 'Estadisticas',
      '/imagenes': 'Imagenes'
    };

    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
      const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
      return (
        <Breadcrumb.Item key={url}>
          <Link to={url}>
            {breadcrumbNameMap[url]}
          </Link>
        </Breadcrumb.Item>
      );
    });

    const breadcrumbItems = [(
      <Breadcrumb.Item key="/">
        <Link to="/"><Icon type="home" />   Inicio</Link>
      </Breadcrumb.Item>
    )].concat(extraBreadcrumbItems);


    return (
      <Breadcrumb>
        {breadcrumbItems}
      </Breadcrumb>
    );
  }
}
