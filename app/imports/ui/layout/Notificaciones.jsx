import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import speechSynthesis from 'speech-synthesis';

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

//Ant Design
import { notification, Icon } from 'antd';

export class Notificaciones extends Component {

  componentDidUpdate(prevProps,nextProps){

    // Si no hay usuario conectado
    if(!this.props.usuarioActual){
      notification.open({
        key: 'notificacion:inicial',
        message: 'Información Importante',
        description:'Nombre de Usuario: Administrador Contraseña: admin001',
        placement:'bottomRight',
        duration: 0,
        icon: <Icon type="info-circle" style={{ color: '#0e77ca' }} />,
      });
    } else {

      //Si es que hay usuario, y se acaba de conectar(previamente era falso)
      if(!prevProps.usuarioActual){
        notification.close('notificacion:inicial');
       speechSynthesis("Bienvenido a SmartControl", "es-MX");
      }
      //  notification.close('notificacion:inicial');
    //    speechSynthesis("Bienvenido a SmartControl", "es-MX");

      //Si es que hay cambios en las conexiones de arduinos, envia notificacion, sino, ni pedos
      if(prevProps.arduinos!==this.props.arduinos){
          if(this.props.arduinos>prevProps.arduinos){
            speechSynthesis('Se ha conectado un nuevo arduino',"Diego");
            notification.open({
              message: 'Hay ' + this.props.arduinos.length + ' Arduinos Conectados',
              placement:'bottomRight',
              icon: <Icon type="check-circle" style={{ color: 'green' }} />,
            });
          } else if(this.props.arduinos<prevProps.arduinos) {
            speechSynthesis('Se ha desconectado un arduino', "es-MX");
            notification.open({
              message: 'Se ha desconectado un arduino',
              placement:'bottomRight',
              icon: <Icon type="check-circle" style={{ color: 'red' }} />,
            });
          }
      }


      //Si es que ha habido un nuevo evento
      if(prevProps.cantidadEventos!==this.props.cantidadEventos && prevProps.cantidadEventos!==0){

        //En caso de que suba la cantidad de eventos, enviar distintas notificaciones
        if(this.props.cantidadEventos>prevProps.cantidadEventos){
          speechSynthesis(this.props.ultimoEvento.tipo, "es-MX");
          if(this.props.ultimoEvento.tipo==='Tag Desconocido'){


            notification.open({
              icon: <Icon className='animated rotateIn infinite' type="question-circle" style={{ color: 'grey' }} />,
            message: <span> Tag Desconocido </span>,
              description: <span><b>UID:</b>{this.props.ultimoEvento.uid}</span>,
              placement:'bottomRight',
              style: {
                //backgroundColor: 'grey',
                //color: 'white',
              },
            });

          } else if(this.props.ultimoEvento.tipo==='Tag Registrado'){
              notification.open({
                icon: <Icon className='animated pulse infinite' type="exclamation-circle" style={{ color: 'orange' }} />,
              message: <span> Tag Registrado </span>,
                description: <span><b>UID:</b>{this.props.ultimoEvento.uid}</span>,
                placement:'bottomRight',
                style: {
                  //backgroundColor: 'grey',
                  //color: 'white',
                },
              });

            } else if (this.props.ultimoEvento.tipo==='Entrada'){

            notification.open({
              icon: <Icon className='animated fadeInUp infinite' type="up-circle" style={{ color: 'green' }} />,
            message: <span> Entrada </span>,
              description: <span><b>UID:</b>{this.props.ultimoEvento.uid}</span>,
              placement:'bottomRight',
              style: {
                //backgroundColor: 'green',
                //color: 'white',
              },
            });

          } else if (this.props.ultimoEvento.tipo==='Salida'){

            notification.open({
              icon: <Icon className='animated fadeInDown infinite' type="down-circle" style={{ color: 'red' }} />,
            message: <span> Salida </span>,
              description: <span><b>UID:</b>{this.props.ultimoEvento.uid}</span>,
              placement:'bottomRight',
              style: {
                //backgroundColor: 'red',
                //color: 'white',
              },
            });
          }
        }

      }


    }


  }

  render() {
    return (
      <div></div>
    );
  }

}

export default createContainer(() => {

    //Se subscribe a la coleccion de eventos
  Meteor.subscribe('eventos');
  Meteor.subscribe('arduinos');

  return {
    arduinos: Arduinos.find({}, { sort: { fecha: -1 } }).fetch(),
    usuarioActual: Meteor.user(),
    cantidadEventos: Eventos.find({}, { sort: { fecha: -1 } }).count(),
    ultimoEvento: Eventos.findOne({}, { sort: { fecha: -1 } }),
  };
}, Notificaciones);
