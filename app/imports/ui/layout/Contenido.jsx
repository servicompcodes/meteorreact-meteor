import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'

//Componentes para enrutar aplicacion
import { withRouter, Switch, Route } from 'react-router-dom';
// Rutas de carga dinamica
import { rutas } from '../../startup/client/Routes.jsx';

import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

//Contenido que se carga dinamicamente
import NotFound from './contenido/NotFound.jsx';

import {Animated} from "react-animated-css";

@withRouter
export class Contenido extends Component {

  render() {

    var loggedInOrNotContent=this.props.usuarioActual?
    <Switch>
      {rutas.map((ruta, i) =>
         <FancyRoute key={i} {...ruta} />
      )}

      <Route component={NotFound} />
    </Switch>:'';


    return (loggedInOrNotContent);
  }
}

class FancyRoute extends Component {
  componentWillMount () {
    nprogress.start()
  }

  componentDidMount () {
    nprogress.done()
  }

  render () {
    return (
      <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
        <Route {...this.props} />
      </Animated>

    )
  }
}

export default createContainer(() => {
  return {
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, Contenido);
