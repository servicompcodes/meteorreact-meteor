import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { withRouter, Switch, Route } from 'react-router-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Menu } from 'antd';
const SubMenu = Menu.SubMenu;

export class SidebarMenu extends Component {

  constructor() {
     super();
     this.state = {
       selectedKeys: []
     }
   }

 static contextTypes = {
   router: React.PropTypes.object
 }

 componentWillReceiveProps(nextProps, nextContext){
   this.setState({ selectedKeys: [nextContext.router.route.location.pathname] });
 }

 componentDidMount() {
   this.setState({ selectedKeys: [this.context.router.route.location.pathname] });
 }

  render() {
    return (
      <Menu
          theme="dark"
          defaultSelectedKeys={['1']}
          selectedKeys={this.state.selectedKeys}
          mode="inline"
        >
          <Menu.Item key="/">
              <span className="nav-text">
                <i className="fa fa-home"/> Inicio
              </span>
              <Link to='/'/>
          </Menu.Item>

          <Menu.Item key="/arduinos">
              <span className="nav-text">
                <i className="fa fa-star"/> Arduinos
              </span>
            <Link to='/arduinos'/>
          </Menu.Item>

            <Menu.Item key="/eventos">
                <span className="nav-text">
                    <i className="fa fa-clock-o"/> Eventos
                </span>
                <Link to='/eventos'/>
            </Menu.Item>

  <SubMenu key="tags" title={<span><i className="fa fa-address-card"/> Tags</span>}>


            {this.props.nombreUsuarioActual=='Gestor'||this.props.nombreUsuarioActual=='Administrador'?

          <Menu.Item key="/tags">
              <span className="nav-text">
                  <i className="fa fa-address-card"/> Tags
              </span>
            <Link to='/tags'/>
          </Menu.Item>:''}

          {this.props.nombreUsuarioActual=='Gestor'||this.props.nombreUsuarioActual=='Administrador'?

          <Menu.Item key="/permisos">
              <span className="nav-text">
                  <i className="fa fa-clock-o"/> Permisos
              </span>
            <Link to='/permisos'/>
          </Menu.Item>:''}

          {this.props.nombreUsuarioActual=='Gestor'||this.props.nombreUsuarioActual=='Administrador'?
            <Menu.Item key="/trabajadores">
              <span className="nav-text">
                  <i className="fa fa-user"/> Trabajadores
              </span>
            <Link to='/trabajadores'/>
          </Menu.Item>:''}

          {this.props.nombreUsuarioActual=='Gestor'||this.props.nombreUsuarioActual=='Administrador'?
            <Menu.Item key="/automoviles">
              <span className="nav-text">
                  <i className="fa fa-car"/> Automoviles
              </span>
            <Link to='/automoviles'/>
          </Menu.Item>:''}

          <Menu.Item key="/visitas">
              <span className="nav-text">
                  <i className="fa fa-address-card-o"/> Visitas
              </span>
            <Link to='/visitas'/>
          </Menu.Item>



  </SubMenu>





          {this.props.nombreUsuarioActual=='Administrador'?
          <Menu.Item key="/estadisticas">
              <span className="nav-text">
                  <i className="fa fa-line-chart"/> Estadisticas
              </span>
            <Link to='/estadisticas'/>
          </Menu.Item>:''}

          {this.props.nombreUsuarioActual=='Gestor'||this.props.nombreUsuarioActual=='Administrador'?
          <Menu.Item key="/imagenes">
              <span className="nav-text">
                <i className="fa fa-image"/> Imagenes
              </span>
            <Link to='/imagenes'/>
          </Menu.Item>:''}

        </Menu>
    );
  }
}

export default createContainer(() => {
  return {
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, SidebarMenu);
