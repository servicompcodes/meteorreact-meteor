import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Menu, Icon, Badge } from 'antd';
import { Row, Col } from 'antd';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

import VisitasBadge from './contenido/Visitas/VisitasBadge.jsx';
import ArduinosBadge from './contenido/Arduinos/ArduinosBadge.jsx';
import EventosBadge from './contenido/Eventos/EventosBadge.jsx';

import Rutas from './navbarmenu/Rutas.jsx';

// NavbarMenu component - represents the whole app
export class NavbarMenu extends Component {

  constructor() {
     super();
     this.state = {
       selectedKeys: []
     }
   }

 static contextTypes = {
   router: React.PropTypes.object
 }

 componentWillReceiveProps(nextProps, nextContext){
   this.setState({ selectedKeys: [nextContext.router.route.location.pathname] });
 }

 componentDidMount() {
   this.setState({ selectedKeys: [this.context.router.route.location.pathname] });
 }

  handleClick = (e) => {
    if(e.key==='/cerrarsesion'){
      Meteor.logout();
    }
  }

  render() {
    return (
      <Row>
        <Col span={8}>
            <Rutas/>
        </Col>

        <Col span={16}>
          <Row type="flex" justify="end">
            <Col>

          <Menu
            onClick={this.handleClick}
            selectedKeys={this.state.selectedKeys}
            mode="horizontal">

              <Menu.Item key="/visitas">
                <VisitasBadge/>
              </Menu.Item>

              <Menu.Item key="/eventos">
                <EventosBadge/>
              </Menu.Item>

              <Menu.Item key="/arduinos">
                <ArduinosBadge/>
              </Menu.Item>

              <SubMenu
                  title={<span><Icon type="user" style={{ fontSize: 14 }} /> Usuario</span>}
                  style={{ left: '50px'}}
                  >

                  <MenuItemGroup title={this.props.nombreUsuarioActual}>

                    <Menu.Item key="/usuarios">
                        <span className="nav-text">
                            <i className="fa fa-user-o"/> Ver Usuarios
                        </span>
                      <Link to='/usuarios'/>
                    </Menu.Item>
                    <Menu.Item key="/miperfil">
                        <span className="nav-text">
                            <i className="fa fa-user"/> Mi Perfil
                        </span>
                      <Link to='/miperfil'/>
                    </Menu.Item>
                    <Menu.Item key="/appconfig">
                        <span className="nav-text">
                            <i className="fa fa-user"/> App Config.
                        </span>
                      <Link to='/appconfig'/>
                    </Menu.Item>
                    <Menu.Item key="/cerrarsesion">
                        <span className="nav-text">
                            <i className="fa fa-sign-out"/> Cerrar Sesion
                        </span>
                      <Link to='/'/>
                    </Menu.Item>
                  </MenuItemGroup>
                </SubMenu>

            </Menu>
            </Col>
          </Row>
          </Col>
        </Row>
     );
  }
}

export default createContainer(() => {
  return {
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, NavbarMenu);
