import React, { Component } from 'react';
import { Row, Col, Form, Input } from 'antd';
const FormItem = Form.Item;

export default class AppConfig extends Component {

  check = () => {
     this.props.form.validateFields(
       (err) => {
         if (!err) {
           console.info('success');
         }
       },
     );
   }


  render() {

    console.log(this);

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
        <Form layout="vertical">
          <FormItem label="UID">
            {getFieldDecorator('uid', {
              rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
            })(
              <Input></Input>
            )}
            </FormItem>


            <Button type="primary" onClick={this.check}>
              Check
            </Button>
        </Form>
    );
  }
}
