import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Badge, Tooltip } from 'antd';

export class ArduinosBadge extends Component {
  render() {
    return (
      <Tooltip placement="bottom" title="Arduinos Conectados">
        <Link to='/arduinos'>
          <Badge count={this.props.cantidadArduinos}>
              <span>
                  <i className="fa fa-star"/>
              </span>
          </Badge>
        </Link>
      </Tooltip>
    );
  }
}

export default createContainer(() => {

  //Se subscribe a la coleccion de arduinos
  Meteor.subscribe('arduinos');

  /*Dentro del objeto return, todos los elementos definidos se convierten en propiedades
    del componente React */
  return {
    cantidadArduinos: Arduinos.find({}, { sort: { fecha: -1 } }).count(),
  };
}, ArduinosBadge);
