import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Row, Col, Tooltip, Affix, Table, Button, Icon, Switch, Radio, Form, notification } from 'antd';
const FormItem = Form.Item;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

const columns = [{
  title: 'ID',
  dataIndex: '_id',
},{
  title: 'Puerto',
  dataIndex: 'puerto',
},{
  title: 'Fabricante',
  dataIndex: 'fabricante',
}, {
  title: 'Serial',
  dataIndex: 'serial',
}, {
  title: 'ID Vendedor',
  dataIndex: 'vendedorID',
}, {
  title: 'ID Producto',
  dataIndex: 'productoID',
}];

export class ArduinosTabla extends Component {

  state = {
    filasSeleccionadas: [],  // Check here to configure the default column
    cargando: false,
  };

  eliminarSeleccionados = () => {

    //Setea el estado de cargando a la tabla
    this.setState({ cargando: true });
    // ajax request after empty completing
    Meteor.call('arduinos.eliminarSeleccionados',this.state.filasSeleccionadas);

    notification.open({
      message: 'Arduinos Eliminados',
      description:'Arduinos eliminados exitosamente',
      placement:'bottomRight',
      icon: <Icon type="delete" style={{ color: 'red' }} />,
    });

    this.setState({
      filasSeleccionadas: [],
      cargando: false,
    });

  }

  start = () => {
    this.setState({ cargando: true });
    // ajax request after empty completing

    setTimeout(() => {
      this.setState({
        filasSeleccionadas: [],
        cargando: false,
      });
    }, 1000);
  }

  onSelectChange = (filasSeleccionadas) => {
    this.setState({ filasSeleccionadas });
  }


  leerArduinos(){
      Meteor.call('arduinos.leer');
  }

  /*
    rowKey="_id"
    Esta propiedad dentro de table permite definiar una columna
    la cual será la "key" que necesita el componente para identificar
    los elementos seleccionados.
  */

  render() {

      const { cargando, filasSeleccionadas } = this.state;

    const rowSelection = {
      filasSeleccionadas,
      onChange: this.onSelectChange,
    };

    const haySeleccionados = filasSeleccionadas.length > 0;

    return (
      <div>
                <Row>
                  <Col span={12}>
                    <h1 style = {{display : "inline"}}> <i className="fa fa-star"/> Arduinos </h1> <h3 style={{display: "inline"}}> Conectados</h3>

                    <span >
                      {haySeleccionados ? ' - '+filasSeleccionadas.length+' Elemento(s) seleccionado(s)' : ''}
                    </span>

                  </Col>


                  <Col span={12}>
                    <Affix offsetTop={12}>
                      <Row type="flex" justify="end" gutter={8} >
                        {
                        this.props.nombreUsuarioActual=='Guardia'?
                          '':
                          <Col >
                            <Tooltip placement="bottom" title={'Leer Puertos COM'}>
                                <Button
                                  size="large"
                                  type="success"
                                  onClick={this.leerArduinos}
                                  loading={cargando}
                                  >
                                  <Icon type="wifi" />
                                </Button>
                             </Tooltip>
                          </Col>
                        }


                        </Row>
                      </Affix>
                  </Col>

              </Row>
              <br>
              </br>

        <Row>
        <Table
          rowKey="_id"
          rowSelection={rowSelection}
          columns={columns}
          dataSource={this.props.arduinos} />
        </Row>
      </div>
    );
  }
}

// Se definen los datos requeridos
ArduinosTabla.propTypes = {
  arduinos: PropTypes.array.isRequired,
};


// Se obtienen los datos de los arduinos
export default createContainer(() => {

  Meteor.subscribe('arduinos');

  return {
    arduinos: Arduinos.find({}, { sort: { fecha: -1 } }).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, ArduinosTabla);
