import React, { Component } from 'react';

import EventosTimeLine from './Eventos/EventosTimeLine.jsx';
import UltimoEvento from './Eventos/UltimoEvento.jsx';
import Reloj from './App/Reloj.jsx';

import { Row, Col } from 'antd';

export default class Home extends Component {
  render() {
    return (
      <Row gutter={16}>

          <Col xs={24} sm={24} md={14} lg={14}>
            <UltimoEvento/><br/>
          </Col>

          <Col xs={24} sm={24} md={10} lg={10}>
            <Reloj hour12= {false}/><br/>
            <EventosTimeLine/>
          </Col>

      </Row>
    );
  }
}
