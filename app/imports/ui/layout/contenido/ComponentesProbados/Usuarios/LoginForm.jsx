import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Row } from 'antd';
const FormItem = Form.Item;

export class LoginForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {

      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row type="flex" justify="center">
        <Form onSubmit={this.handleSubmit} className="login-form">
          <h1>Iniciar Sesión</h1>
          <br/>
            <FormItem>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: 'Por favor ingresa tu nombre de usuario!' }],
              })(
                <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Nombre de Usuario" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Por favor ingresa tu contraseña!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Contraseña" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(
                <Checkbox>Recuérdame</Checkbox>
              )}
              <a className="login-form-forgot" href="">Olvidaste tu Contraseña?</a>

            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit" className="login-form-button">
              Iniciar Sesión
            </Button>
            </FormItem>
        </Form>
      </Row>
    );
  }
}

export default LoginForm = Form.create({})(LoginForm);
