import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'

//Componentes para enrutar aplicacion
import { Switch, Route, Redirect } from 'react-router-dom';

/*Componentes comprobados por mi que funcionan con Meteor+React
son practicamente copypaste de los códigos de ejemplo que aprecen
en la pagina oficial de ant.design*/

//Botones
import ModalButton from './Botones/ModalButton.jsx';
import PopOverButton from './Botones/PopOverButton.jsx';

//Tablas
import TablaDinamica from './Tablas/TablaDinamica.jsx';
import TablaExtendida from './Tablas/TablaExtendida.jsx';

//Tests
import Tests from './Tests/Tests.jsx';

//Usuarios
import LoginForm from './Usuarios/LoginForm.jsx';

export default class ComponentesProbados extends Component {
  render() {
    return (
      <Switch>
        <Route path='/componentesprobados/modalbutton' component={ModalButton} />
        <Route path='/componentesprobados/popoverbutton' component={PopOverButton} />
        <Route path='/componentesprobados/tabladinamica' component={TablaDinamica} />
        <Route path='/componentesprobados/tablaextendida' component={TablaExtendida} />
        <Route path='/componentesprobados/loginform' component={LoginForm} />
        <Route path='/componentesprobados/tests' component={Tests} />
        <Redirect to="/NotFound"/>
      </Switch>
    );
  }
}
