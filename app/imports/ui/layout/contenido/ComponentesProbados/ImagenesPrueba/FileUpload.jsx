import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';

import { createContainer } from 'meteor/react-meteor-data';

import { Button, Modal, Form, Input, Radio, Select, notification, Upload, Icon } from 'antd';

import FileIndividual from './FileIndividual.jsx';
import { _ } from 'meteor/underscore';

export class FileUpload extends Component {

    state = {
      uploading: [],
      progress: 0,
      inProgress: false,
      previewVisible: false,
      previewImage: ''
    }

    handleCancel = () => this.setState({ previewVisible: false })

    handlePreview = (file) => {
      this.setState({
        previewImage: file.url || file.thumbUrl,
        previewVisible: true,
      });
    }

    uploadIt = (e) => {
      e.preventDefault();

      var self = this;

      if (e.currentTarget.files && e.currentTarget.files[0]) {
        // We upload only one file, in case
        // there was multiple files selected
        var file = e.currentTarget.files[0];

        if (file) {
          let uploadInstance = Images.insert({
            file: file,
            meta: {
            //  locator: self.props.fileLocator,
              userId: Meteor.userId() // Optional, used to check on server for file tampering
            },
            streams: 'dynamic',
            chunkSize: 'dynamic',
            allowWebWorkers: true // If you see issues with uploads, change self to false
          }, false);

          self.setState({
            uploading: uploadInstance, // Keep track of self instance to use below
            inProgress: true // Show the progress bar now
          });

          // These are the event functions, don't need most of them, it shows where we are in the process
          uploadInstance.on('start', function () {
          });

          uploadInstance.on('end', function (error, fileObj) {
          });

          uploadInstance.on('uploaded', function (error, fileObj) {

            // Remove the filename from the upload box
            self.refs['fileinput'].value = '';

            // Reset our state for the next file
            self.setState({
              uploading: [],
              progress: 0,
              inProgress: false
            });
          });

          uploadInstance.on('error', function (error, fileObj) {

          });

          uploadInstance.on('progress', function (progress, fileObj) {

            // Update our progress bar
            self.setState({
              progress: progress
            })
          });

          uploadInstance.start(); // Must manually start the upload
        }
      }
    }


    onFileChange(fileList) {
      // always setState
      this.setState({ fileList: [...fileList] });
    }

  // This is our progress bar, bootstrap styled
  // Remove this function if not needed
  showUploads = () => {

    if (!_.isEmpty(this.state.uploading)) {
      return <div>
        {this.state.uploading.file.name}

        <div className="progress progress-bar-default">
          <div style={{width: this.state.progress + '%'}} aria-valuemax="100"
             aria-valuemin="0"
             aria-valuenow={this.state.progress || 0} role="progressbar"
             className="progress-bar">
            <span className="sr-only">{this.state.progress}% Complete (success)</span>
            <span>{this.state.progress}%</span>
          </div>
        </div>
      </div>
    }
  }

  render() {
    let fileCursors = this.props.docs;



    const { previewVisible, previewImage } = this.state;
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    return (
      <div className="clearfix">
        <Upload
          customRequest={this.uploadIt}
          listType="picture-card"
          fileList={fileCursors}
          onPreview={this.handlePreview}
          disabled={this.state.inProgress}
        >
          {fileCursors.length >= 3 ? null : uploadButton}
        </Upload>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

export default createContainer(() => {
  var handle = Meteor.subscribe('files.images.all');

  return {
    docsReadyYet: handle.ready(),
    docs: Images.find().fetch()// Collection is Images
  };
}, FileUpload);
