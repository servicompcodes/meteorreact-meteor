import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import TestConcrete from './TestConcrete.jsx';
import { Form } from 'antd';


export default class Tests extends Component {
  render() {

    const WrappedTimeRelatedForm = Form.create()(TestConcrete);


    return (<WrappedTimeRelatedForm/>);

  }
}
