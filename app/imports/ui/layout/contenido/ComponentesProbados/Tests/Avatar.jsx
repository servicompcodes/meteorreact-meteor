import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Upload, Icon, message } from 'antd';


// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';


function beforeUpload(file) {
  const isJPG = file.type === 'image/jpeg';
  if (!isJPG) {
    message.error('You can only upload JPG file!');
  }
  const isLt2M = file.size / 256 / 256 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJPG && isLt2M;
}
/*
function uploadFile(file) {


  Images.insert(file, function (err, fileObj) {
    if (err) {
      message.error('ERROR:'+err);
    } else if (fileObj){
      message.info('Archivo subido!:'+fileObj);
    }
  });
  //  Meteor.call('Imagenes.Insertar',file,file.name);
}
*/

export class Avatar extends Component {

  state = {
    loading: false,
  };

  handleChange = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }

    if (info.file.status === 'done') {
      // Get this url from response in real world.
      fileObj = Images.insert(info.file.originFileObj, function (err, fileObj) {
        if (err) {
          message.error('ERROR:'+err);
        } else if (fileObj){
          message.info('Archivo subido!:'+fileObj);
        }
      });
      this.setState({ imageUrl: "/cfs/files/images/"+fileObj._id });

    }
  }
  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const imageUrl = this.state.imageUrl;
    return (
      <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        data={this.uploadFile}
        action=""
        beforeUpload={this.beforeUpload}
        onChange={this.handleChange}
      >
        {imageUrl ? <img src={imageUrl} alt="" /> : uploadButton}
      </Upload>
    );
  }
}

// Se obtienen los datos de los usuarios
export default createContainer(() => {

  Meteor.subscribe('imagenes');

  return {
    ultimaImagen: Images.find().fetch(),

  };
}, Avatar);
