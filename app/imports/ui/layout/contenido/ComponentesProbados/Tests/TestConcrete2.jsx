import { Upload, Button, Icon } from 'antd';
import React, { Component } from 'react';

import { createContainer } from 'meteor/react-meteor-data';

const fileList = [{
  uid: '-1',
  name: 'xxx.png',
  status: 'done',
  url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
}, {
  uid: '-2',
  name: 'yyy.png',
  status: 'done',
  url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
}];



export class TestConcrete2 extends Component {

  uploadFile = (e) => {
    const upload = Images.insert({
      file: e.file,
      streams: 'dynamic',
      chunkSize: 'dynamic'
    }, false);

    upload.on('end', function (error, fileObj) {
      if (error) {
        alert('Error during upload: ' + error);
      } else {
        alert('File "' + fileObj.name + '" successfully uploaded');
      }
    });
    upload.start();
  }

  onFileChange(fileList) {

    // always setState
  //  this.setState({ fileList: this.props.imagenes });
  }


  render() {

    return (
      <div>
        <Upload
            customRequest = {this.uploadFile}
            listType = 'picture'
            onChange={this.onFileChange}
          >
          <Button>
            <Icon type="upload" /> Upload
          </Button>
        </Upload>
      </div>
    );
  }
}

// Se obtienen los datos de los automoviles
export default createContainer(() => {
  Meteor.subscribe('files.images.all');

  return {
    imagenes: Images.find({}, {}).fetch(),
  };
}, TestConcrete2);
