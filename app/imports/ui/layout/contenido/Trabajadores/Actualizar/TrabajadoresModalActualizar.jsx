import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Modal, notification, Button, Icon, Tooltip } from 'antd';
import TrabajadoresFormActualizar from './TrabajadoresFormActualizar.jsx'

import { createContainer } from 'meteor/react-meteor-data';

export class TrabajadoresModalActualizar extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false,
      loading: false
    };
  }

    showModal = () => {
      this.setState({ visible: true });
    }

    componentDidUpdate(prevProps, prevState, snapshot){

        var uids = this.props.trabajadores
          .filter( (trabajador) => {
              return this.props.uidSelect.find( (id) =>
                      {	return id==trabajador._id }
                      )?true:false;
          })
          .map( (trabajador) => {
            return this.props.uidSelect.find( (id) =>
                        {	return id==trabajador._id }
                      )?trabajador.uid:'';
          });

        this.formRef?this.formRef.props.form.setFieldsValue({uid:uids}):'';

    }



   handleCancel = () => {
     this.setState({ visible: false });
   }

     handleCreate = () => {
       const form = this.formRef.props.form;
       form.validateFields((err, values) => {
         if (err) {
           return;
         } else {
           Meteor.call('trabajadores.actualizar',values.uid,values.rut,values.cargo,values.nombre,values.apellidop,values.apellidom);

             notification['success']({
               message: 'Trabajador Ingresado',
               description:'Trabajador ingresado exitosamente',
               placement:'bottomRight',
             });

           form.resetFields();
           this.setState({ visible: false });

         }

     });
   }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }


    uploadIt = (e) => {
      e.preventDefault();


      if (e.currentTarget.files && e.currentTarget.files[0]) {
        // We upload only one file, in case
        // there was multiple files selected
        var file = e.currentTarget.files[0];

        this.setState({file:file});


      }
    }

  render() {

   const EnhancedForm =  Form.create()(TrabajadoresFormActualizar);

    return (<div>

      <Tooltip placement="bottom" title={'Actualizar'}>
        <Button
          type="primary"
          size="large"
          disabled={!this.props.haySeleccionados}
          onClick={this.showModal}
        >
          <Icon type="sync" />
        </Button>
      </Tooltip>

      <Modal
        visible={this.state.visible}
        title="Actualizar Trabajador"
        okText="Actualizar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
      <h4>Imagen:</h4>
      <input
        type="file"
        id="fileinput"
        disabled={this.state.inProgress}
        ref="fileinput"
        onChange={this.uploadIt}
      /><br/>
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />
      </Modal>
    </div>

   );
  }
}

// Se obtienen los datos de los trabajadores
export default createContainer(() => {
  Meteor.subscribe('trabajadores');
  return {
    trabajadores: Trabajadores.find({}, { sort: { fecha: -1 } }).fetch()
  };
}, TrabajadoresModalActualizar);
