import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Button, Modal, Form, Input, Radio, Select, notification, Upload, Icon } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

function formatearRut(value) {

  return value;
}

export default class TrabajadoresFormActualizar extends Component {

    constructor(props){
      super(props);
      this.state = {
        rut: ''
      };
    }

    normalizarRut = (value, prevValue = []) => {

      const reg = /^[0-9]?[0-9]?\.?[0-9]?[0-9]?[0-9]?\.?[0-9]?[0-9]?[0-9]?[-]?[0-9kK]?$/;
      if (reg.test(value)) {
        return formatearRut(value);
      }
    }


  render() {
    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    const { rut } = this.state;

    return (
      <Form layout="vertical">

        <FormItem label="UID">
          {getFieldDecorator('uid', {
            rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
          })(
            <Select
              disabled
              mode="multiple"
              placeholder="Seleccione un TAG"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
            </Select>
          )}
          </FormItem>

        <FormItem label="RUT">
          {getFieldDecorator('rut', {
            rules: [{ required: true, message: 'Por favor ingrese un rut', max:12 }],
            normalize: this.normalizarRut,
          })(
            <Input

              />
          )}
        </FormItem>
        <FormItem label="Cargo">
          {getFieldDecorator('cargo', {
            rules: [{ required: true, message: 'Por favor seleccione un cargo' }],
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Seleccione un cargo"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="Obrero">Obrero</Option>
              <Option value="Ingeniero">Ingeniero</Option>
              <Option value="Topografo">Topografo</Option>
            </Select>
          )}
        </FormItem>
        <FormItem label="Nombre">
          {getFieldDecorator('nombre', {
            rules: [{ required: true, message: 'Por favor el nombre' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem label="Apellido Paterno">
          {getFieldDecorator('apellidop', {
            rules: [{ required: true, message: 'Por favor ingrese el apellido paterno' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem label="Apellido Materno">
          {getFieldDecorator('apellidom', {
            rules: [{ required: true, message: 'Por favor ingrese el apellido materno' }],
          })(
            <Input />
          )}
        </FormItem>
      </Form>
   );
  }

}
