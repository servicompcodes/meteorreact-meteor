import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Modal, notification, Button, Icon, Tooltip } from 'antd';
import TrabajadoresForm from './TrabajadoresForm.jsx'

export default class TrabajadoresModal extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false,
      loading: false
    };
  }

    showModal = () => {
      this.setState({ visible: true });
    }

   componentWillReceiveProps(nextProps, nextContext){
     this.setState({ visible: nextProps.visible });
   }

   handleCancel = () => {
     this.setState({ visible: false });
   }

     handleCreate = () => {
       const form = this.formRef.props.form;
       form.validateFields((err, values) => {
         if (err) {
           return;
         }

         Meteor.call('trabajadores.insertar',values.uid,values.rut,values.cargo,values.nombre,values.apellidop,values.apellidom);

         var self = this;

         if (this.state.file) {
             let uploadInstance = Images.insert({
               file: this.state.file,
               meta: {
               //  locator: self.props.fileLocator,
                 userId: Meteor.userId(), // Optional, used to check on server for file tampering
                 fecha: new Date(),
                 uid: values.uid,
               },
               streams: 'dynamic',
               chunkSize: 'dynamic',
               allowWebWorkers: true // If you see issues with uploads, change self to false
             }, false);

             self.setState({
               uploading: uploadInstance, // Keep track of self instance to use below
               inProgress: true // Show the progress bar now
             });

             // These are the event functions, don't need most of them, it shows where we are in the process
             uploadInstance.on('start', function () {

             });

             uploadInstance.on('end', function (error, fileObj) {

               self.setState({
                 cargado:true
               });

             });

             uploadInstance.on('uploaded', function (error, fileObj) {


               // Remove the filename from the upload box
               self.refs['fileinput'].value = '';

               // Reset our state for the next file
               self.setState({
                 uploading: [],
                 progress: 0,
                 inProgress: false
               });
             });

             uploadInstance.on('error', function (error, fileObj) {

             });

             uploadInstance.on('progress', function (progress, fileObj) {

               // Update our progress bar
               self.setState({
                 progress: progress
               })
             });

             uploadInstance.start(); // Must manually start the upload


           notification['success']({
             message: 'Trabajador Ingresado',
             description:'Trabajador ingresado exitosamente',
             placement:'bottomRight',
           });


         form.resetFields();
         this.setState({ visible: false });
       }
     });
   }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }


    uploadIt = (e) => {
      e.preventDefault();


      if (e.currentTarget.files && e.currentTarget.files[0]) {
        // We upload only one file, in case
        // there was multiple files selected
        var file = e.currentTarget.files[0];

        this.setState({file:file});


      }
    }

  render() {

   const EnhancedForm =  Form.create()(TrabajadoresForm);

    return (<div>
      <Tooltip placement="bottom" title={'Ingresar'}>
      <Button
        size="large"
        type="success"
        onClick={this.showModal}
        >
        <Icon type="plus-circle-o" />
      </Button>
    </Tooltip>

      <Modal
        visible={this.state.visible}
        title="Ingresar Trabajador"
        okText="Ingresar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
      <h4>Imagen:</h4>
      <input
        type="file"
        id="fileinput"
        disabled={this.state.inProgress}
        ref="fileinput"
        onChange={this.uploadIt}
      /><br/>
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />
      </Modal>
    </div>

   );
  }
}
