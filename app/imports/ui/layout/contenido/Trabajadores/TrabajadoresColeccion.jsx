import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import TrabajadoresTabla from './TrabajadoresTabla.jsx';

export default class TrabajadoresColeccion extends Component {
  render() {
    return (<TrabajadoresTabla/>);
  }
}
