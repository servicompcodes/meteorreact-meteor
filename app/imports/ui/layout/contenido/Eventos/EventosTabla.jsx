import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Row, Col, Affix, Tooltip, Table, Button, Icon, Switch, Radio, Form, notification } from 'antd';
const FormItem = Form.Item;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';


const columns = [{
  title: 'UID',
  dataIndex: 'uid',
}, {
  title: 'Tipo',
  dataIndex: 'tipo',
},{
  title: 'Estado',
  dataIndex: 'estado',
},{
  title: 'Arduino',
  dataIndex: 'arduino',
},{
  title: 'Atraso(Min.)',
  dataIndex: 'atraso',
},{
  title: 'Fecha',
  dataIndex: 'fecha',
  key: 'fecha',
  render: function (text, record, index) {
    return record.fecha.toLocaleString();
  }
}];

export class EventosTabla extends Component {

  state = {
    filasSeleccionadas: [],  // Check here to configure the default column
    loading: false,
  };

  eliminarSeleccionados = () => {

    //Setea el estado de cargando a la tabla
    this.setState({ loading: true });
    // ajax request after empty completing
    Meteor.call('eventos.eliminarSeleccionados',this.state.filasSeleccionadas);

    notification.open({
      message: 'Eventos Eliminados',
      description:'Eventos eliminados exitosamente',
      placement:'bottomRight',
      icon: <Icon type="delete" style={{ color: 'red' }} />,
    });

    this.setState({
      filasSeleccionadas: [],
      loading: false,
    });

  }

  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing

    setTimeout(() => {
      this.setState({
        filasSeleccionadas: [],
        loading: false,
      });
    }, 1000);
  }

  onSelectChange = (filasSeleccionadas) => {
    this.setState({ filasSeleccionadas });
  }

  /*
    rowKey="_id"
    Esta propiedad dentro de table permite definiar una columna
    la cual será la "key" que necesita el componente para identificar
    los elementos seleccionados.
  */

  render() {

      const { loading, filasSeleccionadas } = this.state;

    const rowSelection = {
      filasSeleccionadas,
      onChange: this.onSelectChange,
    };

    const hasSelected = filasSeleccionadas.length > 0;

    return (
      <div>
        <Row>
          <Col span={12}>
            <h1 style = {{display : "inline"}}><i className="fa fa-clock-o"/> Eventos </h1>

            <span >
              {hasSelected ? ' - '+filasSeleccionadas.length+' Elemento(s) seleccionado(s)' : ''}
            </span>

          </Col>


          <Col span={12}>
            <Affix offsetTop={12}>
              <Row type="flex" justify="end" gutter={8} >
                {
                this.props.nombreUsuarioActual=='Guardia'?
                  '':
                  <Col >
                    <Tooltip placement="bottom" title={'Eliminar'}>
                     <Button
                       type="danger"
                       size="large"
                       onClick={this.eliminarSeleccionados}
                       disabled={!hasSelected}
                       loading={loading}
                     >
                       <Icon type="minus-circle-o" />
                     </Button>
                   </Tooltip>
                  </Col>
                }

                </Row>
              </Affix>
          </Col>

      </Row>
      <br>
      </br>

      <Row>
        <Table
          rowKey="_id"
          rowSelection={rowSelection}
          columns={columns}
          dataSource={this.props.eventos} />
      </Row>
      </div>
    );
  }
}

// Se definen los datos requeridos
EventosTabla.propTypes = {
  eventos: PropTypes.array.isRequired,
};


// Se obtienen los datos de los eventos
export default createContainer(() => {

  Meteor.subscribe('eventos');

  return {
    eventos: Eventos.find({}, { sort: { fecha: -1 } }).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, EventosTabla);
