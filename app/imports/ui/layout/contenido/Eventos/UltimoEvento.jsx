import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import { createContainer } from 'meteor/react-meteor-data';

import { Icon, Card } from 'antd';
import moment from 'moment';

import {Animated} from "react-animated-css";


export class UltimoEvento extends Component {

  render() {
    const elemento = this.props.ultimoEvento.length>0?this.props.ultimoEvento.map((evento,index) => {

      var ultimoEvento = evento;
      var uid = ultimoEvento.uid;
      var auto = this.props.automoviles.find(auto => auto.uid === uid);
      var trab = this.props.trabajadores.find(trabajador => trabajador.uid === uid);
      var vis = this.props.visitas.find(visita => visita.uid === uid);

      var icon;
      var tipo = ultimoEvento.tipo;
      if(tipo==='Entrada'){
        icon = <Icon type="up-circle" style={{ color: 'green' }} />;
      } else if(tipo==='Salida'){
        icon = <Icon type="down-circle" style={{ color: 'red' }} />;
      } else if(tipo==='Tag Desconocido'){
        icon = <Icon  type="question-circle" style={{ color: 'grey' }} />;
      }else if(tipo==='Tag Registrado'){
        icon = <Icon  type="exclamation-circle" style={{ color: 'orange' }} />;
      }
/*
      if(this.props.docsReadyYet){
        var imgLink = this.props.docs.find({'meta.uid': uid})[0].link();  //The "view/download" link
      }
*/
var link;

if(this.props.docsReadyYet){
  link = Images.findOne({'meta.uid': uid})?Images.findOne({'meta.uid': uid}).link():"https://via.placeholder.com/250x300";
}

      if(auto){
        return(<div key={index}>
          <h1>{ultimoEvento?icon:''} {ultimoEvento?'Automovil':'No Existe Ultimo Evento'}</h1> <h2>{ultimoEvento? ultimoEvento.fecha.toLocaleString() :''}</h2><br/>
          <img src={link}/><br/>
          <b>UID: </b>  {ultimoEvento?ultimoEvento.uid:''} <br/>
          <b>Arduino: </b>{ultimoEvento?ultimoEvento.arduino:''}<br/>

          <b>Patente: </b>{auto.patente}<br/>
          <b>Marca: </b>{auto.marca}<br/>
          <b>Modelo: </b>{auto.modelo}<br/>
          <b>Color: </b>{auto.color}<br/>
      </div>);
      } else if (trab){
        return(<div key={index}>
          <h1>{ultimoEvento?icon:''} {ultimoEvento?'Trabajador':'No Existe Ultimo Evento'}</h1> <h2>{ultimoEvento?ultimoEvento.fecha.toLocaleString():''}</h2><br/>
          <img src={link}/><br/>
          <b>UID: </b>  {ultimoEvento?ultimoEvento.uid:''} <br/>
          <b>Arduino: </b>{ultimoEvento?ultimoEvento.arduino:''}<br/>

          <b>Rut: </b>{trab.rut}<br/>
          <b>Nombre: </b>{trab.nombre}<br/>
          <b>Apellido P: </b>{trab.apellidop}<br/>
          <b>Apellido M: </b>{trab.apellidom}<br/>
          <b>Cargo: </b>{trab.cargo}<br/>
        </div>);
      } else if (vis) {
        return(<div key={index}>
          <h1>{ultimoEvento?icon:''} {ultimoEvento?'Visita':'No Existe Ultimo Evento'}</h1> <h2>{ultimoEvento?ultimoEvento.fecha.toLocaleString():''}</h2><br/>
            <img src={link}/><br/>
          <b>UID: </b>  {ultimoEvento?ultimoEvento.uid:''} <br/>
          <b>Arduino: </b>{ultimoEvento?ultimoEvento.arduino:''}<br/>

          <b>Rut: </b>{vis.rut}<br/>
          <b>Nombre: </b>{vis.nombre}<br/>
        </div>);
      } else if (tipo==='Tag Desconocido') {
        return(<div key={index}>
          <h1>{ultimoEvento?icon:''} {ultimoEvento?'Tag Desconocido':'No Existe Ultimo Evento'}</h1> <h2>{ultimoEvento?ultimoEvento.fecha.toLocaleString():''}</h2><br/>
            <img src={link}/><br/>
          <b>UID: </b>  {ultimoEvento?ultimoEvento.uid:''} <br/>
          <b>Arduino: </b>{ultimoEvento?ultimoEvento.arduino:''}<br/>

          <b>DESCONOCIDO</b>
        </div>);
      } else if (tipo==='Tag Registrado') {
        return(<div key={index}>
          <h1>{ultimoEvento?icon:''} {ultimoEvento?'Tag Registrado':'No Existe Ultimo Evento'}</h1> <h2>{ultimoEvento? moment(ultimoEvento.fecha).format('DD of MMMM of YYYY, HH:mm:ss'):''}</h2><br/>
            <img src={link}/><br/>
          <b>UID: </b>  {ultimoEvento?ultimoEvento.uid:''} <br/>
          <b>Arduino: </b>{ultimoEvento?ultimoEvento.arduino:''}<br/>

          <b>Registrado</b>
        </div>);
      }

    },this):'No Existen Eventos';

    return (
          <center>
            <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>

            <Card
              title="Ultimo Evento"
              bordered={true}
              >
              {elemento}
            </Card>
            </Animated>
          </center>
    );
  }
}


export default createContainer(() => {

  Meteor.subscribe('eventos');
  Meteor.subscribe('automoviles.all');
  Meteor.subscribe('trabajadores');
  Meteor.subscribe('visitas');
  var handle = Meteor.subscribe('files.images.all');


  /*Dentro del objeto return, todos los elementos definidos se convierten en propiedades
    del componente React */
  return {
    ultimoEvento: Eventos.find({},{sort : {fecha:-1}, limit:1}).fetch(),
    automoviles: Automoviles.find().fetch(),
    trabajadores: Trabajadores.find().fetch(),
    visitas: Visitas.find().fetch(),
    docsReadyYet: handle.ready(),
    docs: Images.find().fetch()// Collection is Images
  };
}, UltimoEvento);
