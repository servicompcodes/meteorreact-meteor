import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

import { Card, Tooltip, Timeline, Icon } from 'antd';

import moment from 'moment';
import {Animated} from "react-animated-css";


export class EventosTimeLine extends Component {

  render() {
    const eventoItem = this.props.eventos.map((evento,index)  => {

      var auto = this.props.automoviles.find(auto => auto.uid === evento.uid);
      var trab = this.props.trabajadores.find(trabajador => trabajador.uid === evento.uid);
      var vis = this.props.visitas.find(visita => visita.uid === evento.uid);

      var elemento;

      if(auto){
        elemento = <span><Tooltip placement="left" title="Automovil"><i className="fa fa-car"/> </Tooltip> {auto.marca} {auto.modelo} {auto.color} - {auto.patente}</span>;
      } else if (trab){
        elemento = <span><Tooltip placement="left" title="Trabajador"><i className="fa fa-user"/> </Tooltip> {trab.nombre} {trab.apellidop} - {trab.rut}</span>;
      } else if (vis) {
        elemento = <span><Tooltip placement="left" title="Trabajador"><i className="fa fa-user"/> </Tooltip>{vis.nombre} {vis.apellido} - {vis.rut}</span>;
      } else if (evento.tipo == 'Tag Desconocido') {
        elemento = <span>Tag Desconocido - {evento.uid}</span>;
      } else if (evento.tipo == 'Tag Registrado') {
        elemento = <span>Tag Registrado - {evento.uid}</span>;
      }

      if (evento.tipo == 'Entrada'){
        return(
          <Timeline.Item
            key={index}
            dot={<Icon type="up-circle" style={{ fontSize: '16px' }}/>}
            color="green">
            <Tooltip placement="top" title={evento.fecha.toDateString()}>
              {elemento}
            </Tooltip>
          </Timeline.Item>
        )
      } else if (evento.tipo == 'Salida'){
        return(
          <Timeline.Item
            key={index}
            dot={<Icon type="down-circle" style={{ fontSize: '16px' }}/>}
            color="red">
            <Tooltip placement="top" title={evento.fecha.toDateString()}>
              {elemento}
            </Tooltip>
          </Timeline.Item>
        )
      } else if (evento.tipo == 'Tag Desconocido'){
        return(
          <Timeline.Item
            key={index}
            dot={<Icon type="question-circle" style={{ fontSize: '16px' }}/>}
            color="grey">
            <Tooltip placement="top" title={evento.fecha.toDateString()}>
              {elemento}
            </Tooltip>
          </Timeline.Item>
        )
      } else if (evento.tipo == 'Tag Registrado'){
        return(
          <Timeline.Item
            key={index}
            dot={<Icon type="exclamation-circle" style={{ fontSize: '16px', color: 'orange' }} />}
            color="yellow">
            <Tooltip placement="top" title={evento.fecha.toDateString()}>
              {elemento}
            </Tooltip>
          </Timeline.Item>
        )
      }
    },this);

    return (
      <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>

      <Card
        title="Eventos Timeline"
        bordered={true}
        >
          <Timeline>
            {eventoItem}
          </Timeline>
      </Card>
    </Animated>

    );
  }
}

// Se obtienen los datos de los eventos
export default createContainer(() => {
  Meteor.subscribe('eventos');
  Meteor.subscribe('automoviles.all');
  Meteor.subscribe('trabajadores');
  Meteor.subscribe('visitas');

  return {
    eventos: Eventos.find({},{sort : {fecha:-1}, limit:10}).fetch(),

    automoviles: Automoviles.find().fetch(),

    trabajadores: Trabajadores.find().fetch(),

    visitas: Visitas.find().fetch(),
    //visitas: Eventos.map((evento,index)  => {return Visitas.find(uid:evento.uid).fetch()})

  };
}, EventosTimeLine);
