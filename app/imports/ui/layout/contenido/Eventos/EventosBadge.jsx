import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Badge,Tooltip } from 'antd';

export class EventosBadge extends React.Component {
  render() {
    return (
      <Tooltip placement="bottom" title="Eventos">
        <Link to='/eventos'>
          <Badge count={this.props.cantidadEventos}>
            <span>
              <i className="fa fa-clock-o"/>
            </span>
          </Badge>
        </Link>
      </Tooltip>
    );
  }
}

export default createContainer(() => {

  //Se subscribe a la coleccion de eventos
  Meteor.subscribe('eventos');

  /*Dentro del objeto return, todos los elementos definidos se convierten en propiedades
    del componente React */
  return {
    cantidadEventos: Eventos.find({}, { sort: { fecha: -1 } }).count(),
  };
}, EventosBadge);
