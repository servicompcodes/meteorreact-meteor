import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Tooltip, Affix, Table, Button, Icon, notification } from 'antd';
import { Col, Row } from 'antd';

import UsuariosModal from './UsuariosModal.jsx';

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

const columns = [{
          title: 'ID',
          dataIndex: '_id',
        },{
          title: 'Nombre de Usuario',
          dataIndex: 'username',
        },{
          title: 'Email',
          dataIndex: 'emails.0.address',
      }];

export class UsuariosTabla extends Component {

  state = {
    filasSeleccionadas: [],  // Check here to configure the default column
    cargando: false,
    visible: false,
  };

  componentWillReceiveProps(nextProps, nextContext){
    this.setState({ visible: nextProps.visible });
  }

  eliminarSeleccionados = () => {
    //Setea el estado de cargando a la tabla
    this.setState({ cargando: true });
    // ajax request after empty completing
    Meteor.call('usuarios.eliminarSeleccionados',this.state.filasSeleccionadas, function(error,result){
      if (error){
        message.error("ERROR: "+error);
      } else {

        notification.open({
          message: 'Usuarios Eliminados',
          description:'Usuarios eliminados exitosamente',
          placement:'bottomRight',
          icon: <Icon type="delete" style={{ color: 'red' }} />,
        });
      }
    });

    this.setState({
      filasSeleccionadas: [],
      cargando: false
    });
}

onSelectChange = (filasSeleccionadas) => {
  this.setState({ filasSeleccionadas, visible:false });
}

  showModal = () => {
    this.setState({ visible: true });
  }

  render() {

  const { cargando, filasSeleccionadas } = this.state;

    const rowSelection = {
      filasSeleccionadas,
      onChange: this.onSelectChange,
    };

    const haySeleccionadas = filasSeleccionadas.length > 0;

    return (
      <div>
        <UsuariosModal
          visible={this.state.visible}>
        </UsuariosModal>

          <Row>
            <Col span={12}>
              <h1 style = {{display : "inline"}}> Usuarios </h1>

              <span >
                {haySeleccionadas ? ' - '+filasSeleccionadas.length+' Elemento(s) seleccionado(s)' : ''}
              </span>

            </Col>


            <Col span={12}>
              <Affix offsetTop={12}>

                <Row type="flex" justify="end" gutter={8} >
                  <Col >
                    <Tooltip placement="bottom" title={'Ingresar'}>
                        <Button
                          size="large"
                          onClick={this.showModal}
                          >
                          <Icon type="plus-circle-o" />
                        </Button>
                     </Tooltip>
                  </Col>
                  <Col >
                    <Tooltip placement="bottom" title={'Actualizar'}>
                      <Button
                        type="primary"
                        size="large"
                        onClick={this.showModal}
                        disabled={!haySeleccionadas}
                        loading={cargando}
                      >
                        <Icon type="sync" />
                      </Button>
                    </Tooltip>
                    </Col>
                    <Col >
                      <Tooltip placement="bottom" title={'Eliminar'}>
                       <Button
                         type="danger"
                         size="large"
                         onClick={this.eliminarSeleccionados}
                         disabled={!haySeleccionadas}
                         loading={cargando}
                       >
                         <Icon type="minus-circle-o" />
                       </Button>
                     </Tooltip>
                    </Col>
                  </Row>
                </Affix>

            </Col>

        </Row>
        <br>
        </br>

        <Row>
          <Table
            rowKey="_id"
            rowSelection={rowSelection}
            columns={columns}
            dataSource={this.props.usuarios}
          />
      </Row>
    </div>
    );
  }
}

// Se obtienen los datos de los usuarios
export default createContainer(() => {

  Meteor.subscribe('usuarios');

  return {
    usuarios: Meteor.users.find({}).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, UsuariosTabla);
