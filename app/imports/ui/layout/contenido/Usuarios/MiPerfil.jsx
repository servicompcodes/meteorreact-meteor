import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Col, Row } from 'antd';

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';
export class MiPerfil extends Component {

  render() {
    return (
      <div>

      </div>
    );
  }
}

// Se obtienen los datos de los usuarios
export default createContainer(() => {

  Meteor.subscribe('usuarios');

  return {
    usuarios: Meteor.users.find({}).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, MiPerfil);
