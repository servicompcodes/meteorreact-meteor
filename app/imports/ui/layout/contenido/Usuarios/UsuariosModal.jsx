import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Input, Modal, notification } from 'antd';

import UsuariosForm from './UsuariosForm.jsx'

const FormItem = Form.Item;


export default class UsuariosModal extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false
    };
  }

   componentWillReceiveProps(nextProps, nextContext){
     this.setState({ visible: nextProps.visible });
   }

   handleCancel = () => {
     this.setState({ visible: false });
   }

   handleCreate = () => {

     const form = this.formRef.props.form;

     form.validateFields((err, values) => {
       if (err) {
         return;
       }

       Meteor.call('usuarios.registrar',values.username,values.password,values.email);

       notification['success']({
         message: 'Usuario Registrado',
         description:'Usuario ingresado exitosamente',
         placement:'bottomRight',
       });

     });

     form.resetFields();
     this.setState({ visible: false });
   }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }

  render() {

   const EnhancedForm =  Form.create()(UsuariosForm);

    return (
      <Modal
        visible={this.state.visible}
        title="Registrar Usuario"
        okText="Registrar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />
      </Modal>
   );
  }
}
