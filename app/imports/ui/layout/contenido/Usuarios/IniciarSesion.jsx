import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { message, Form, Icon, Input, Button, Checkbox, Card } from 'antd';

import { Row, Col } from 'antd';
const FormItem = Form.Item;

export class IniciarSesion extends Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        Meteor.loginWithPassword(values.userName, values.password, function(err){
          if (err) {
            message.error("ERROR: "+err);
          } else {
            message.success('Bienvenido!');
          }
        });
      } else {
        message.error("ERROR: "+err);
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row  style={{ margin: '24px 16px', backgroundImage:'url(/images/background.png)', backgroundSize:'contain', minHeight: '95%'}}>
      <Row type="flex" justify="center" align="bottom" style={{ marginTop: '10%'}}>
        <Col>
        <Card style={{ width: 300 }} className='animated flipInY'>
          <img className="animated fadeIn" src="/images/logo_m.png"></img>
          <Form onSubmit={this.handleSubmit} className="login-form">
              <FormItem>
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Por favor ingresa tu nombre de usuario!' }],
                })(
                  <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Nombre de Usuario" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Por favor ingresa tu contraseña!' }],
                })(
                  <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Contraseña" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: true,
                })(
                  <Checkbox>Recuérdame</Checkbox>
                )}
              </FormItem>
              <FormItem>
                <center>
                  <Button size="large" type="primary" htmlType="submit" className="login-form-button">
                    Iniciar Sesión
                  </Button>
                  </center>
              </FormItem>
          </Form>
        </Card>
        </Col>
      </Row>
    </Row>
    );
  }
}

export default IniciarSesion = Form.create({})(IniciarSesion);
