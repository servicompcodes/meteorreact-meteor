import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Modal, notification, Button, Icon, Tooltip } from 'antd';
import PermisosFormActualizar from './PermisosFormActualizar.jsx'

import { createContainer } from 'meteor/react-meteor-data';

export class PermisosModalActualizar extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false
    };
  }


    showModal = () => {
      this.setState({ visible: true });
    }

    componentDidUpdate(prevProps, prevState, snapshot){

        var uids = this.props.permisos
          .filter( (permiso) => {
              return this.props.uidSelect.find( (id) =>
                      {	return id==permiso._id }
                      )?true:false;
          })
          .map( (permiso) => {
            return this.props.uidSelect.find( (id) =>
                        {	return id==permiso._id }
                      )?permiso.uid:'';
          });

        this.formRef?this.formRef.props.form.setFieldsValue({uid:uids}):'';

    }


   handleCancel = () => {
     this.setState({ visible: false });
   }

     handleCreate = () => {
       const form = this.formRef.props.form;
       form.validateFields((err, values) => {
         if (err) {
           return;
         } else {

         Meteor.call('permisos.actualizar',values.uid,values['range-time-picker'][0].toDate(),values['range-time-picker'][1].toDate());

         notification['success']({
           message: 'Permiso Ingresado',
           description:'Permiso ingresado exitosamente',
           placement:'bottomRight',
         });

         form.resetFields();
         this.setState({ visible: false });

        }
      });

     }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }

  render() {

   const EnhancedForm =  Form.create()(PermisosFormActualizar);

    return (<div>

      <Tooltip placement="bottom" title={'Actualizar'}>
        <Button
          type="primary"
          size="large"
          disabled={!this.props.haySeleccionados}
          onClick={this.showModal}
        >
          <Icon type="sync" />
        </Button>
      </Tooltip>

      <Modal
        visible={this.state.visible}
        title="Actualizar Permiso"
        okText="Actualizar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />

      </Modal>

    </div>


   );
  }
}

// Se obtienen los datos de los permisos
export default createContainer(() => {
  Meteor.subscribe('permisos');
  return {
    permisos: Permisos.find({}, { sort: { fecha: -1 } }).fetch()
  };
}, PermisosModalActualizar);
