import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal, Form, Input, Radio, Select, notification, DatePicker } from 'antd';
const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
const Option = Select.Option;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

export default class PermisosForm extends Component {

  render() {

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="vertical">
        <FormItem label="TAG(UID)">
          {getFieldDecorator('uid', {
            rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
          })(
            <Select
              disabled
              mode="multiple"
              placeholder="Seleccione un TAG"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
            </Select>
          )}
        </FormItem>
        <FormItem
          label="Rango de Fecha y Hora"
        >
          {getFieldDecorator('range-time-picker', {
            rules: [{ type: 'array', required: true, message: 'Por Favor seleccione Fecha y Hora correspondiente a este permiso' }],
          })(
            <RangePicker showTime format="YYYY-MM-DD HH:mm:ss" />
          )}
        </FormItem>
      </Form>
   );
  }

}
