import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Table, Button, Icon, Switch, Radio, Form, notification, Col, Row, Affix, Tooltip } from 'antd';
import moment from 'moment';

const FormItem = Form.Item;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

import PermisosModal from './Insertar/PermisosModal.jsx';
import PermisosModalActualizar from './Actualizar/PermisosModalActualizar.jsx';

const columns = [{
  title: 'UID',
  dataIndex: 'uid',
},{
  title: 'Fecha Inicio',
  dataIndex: 'fechayhoraIni',
  key: 'fechaIni',
  render: function (text, record, index) {
    return moment(record.fechayhoraIni).format('DD/MM/YYYY');
  }
},{
  title: 'Fecha Termino',
  dataIndex: 'fechayhoraFin',
  key: 'fechaTermino',
  render: function (text, record, index) {
    return moment(record.fechayhoraFin).format('DD/MM/YYYY');
  }
},{
  title: 'Hora Inicio',
  dataIndex: 'fechayhoraIni',
  key: 'horaIni',
  render: function (text, record, index) {
    return moment(record.fechayhoraIni).format('kk:mm:ss');
  }
},{
  title: 'Hora Termino',
  dataIndex: 'fechayhoraFin',
  key: 'horaFin',
  render: function (text, record, index) {
    return moment(record.fechayhoraFin).format('kk:mm:ss');
  }
}];

export class PermisosTabla extends Component {

  state = {
    filasSeleccionadas: [],  // Check here to configure the default column
    cargando: false,
  };

  eliminarSeleccionados = () => {

    //Setea el estado de cargando a la tabla
    this.setState({ cargando: true });
    // ajax request after empty completing
    Meteor.call('permisos.eliminarSeleccionados',this.state.filasSeleccionadas);

    notification.open({
      message: 'Permisos Eliminados',
      description:'Permisos eliminados exitosamente',
      placement:'bottomRight',
      icon: <Icon type="delete" style={{ color: 'red' }} />,
    });

    this.setState({
      filasSeleccionadas: [],
      cargando: false,
    });

  }

  start = () => {
    this.setState({ cargando: true });
    // ajax request after empty completing

    setTimeout(() => {
      this.setState({
        filasSeleccionadas: [],
        cargando: false,
      });
    }, 1000);
  }

  onSelectChange = (filasSeleccionadas) => {
    this.setState({ filasSeleccionadas });
  }

  /*
    rowKey="_id"
    Esta propiedad dentro de table permite definiar una columna
    la cual será la "key" que necesita el componente para identificar
    los elementos seleccionados.
  */

  render() {

    const { cargando, filasSeleccionadas } = this.state;

    const rowSelection = {
      filasSeleccionadas,
      onChange: this.onSelectChange,
    };

    const haySeleccionados = filasSeleccionadas.length > 0;

    return (
      <div>
        <Row>
          <Col span={12}>
            <h1 style = {{display : "inline"}}><i className="fa fa-clock-o"/> Permisos </h1>
            <span >
              {haySeleccionados ? ' - '+filasSeleccionadas.length+' Elemento(s) seleccionado(s)' : ''}
            </span>
          </Col>


          <Col span={12}>
            <Affix offsetTop={12}>
              <Row type="flex" justify="end" gutter={8} >
                <Col >
                    <PermisosModal/>
                </Col>
                <Col >
                  <PermisosModalActualizar
                        haySeleccionados={haySeleccionados}
                        uidSelect={rowSelection.filasSeleccionadas}
                    />
                  </Col>
                  <Col >
                    <Tooltip placement="bottom" title={'Eliminar'}>
                     <Button
                       type="danger"
                       size="large"
                       onClick={this.eliminarSeleccionados}
                       disabled={!haySeleccionados}
                       loading={cargando}
                     >
                       <Icon type="minus-circle-o" />
                     </Button>
                   </Tooltip>
                  </Col>
                </Row>
              </Affix>
          </Col>

      </Row>
      <br>
      </br>
      <Row>
        <Table
          rowKey="_id"
          rowSelection={rowSelection}
          columns={columns}
          dataSource={this.props.permisos} />
        </Row>
      </div>
    );
  }
}

// Se definen los datos requeridos
PermisosTabla.propTypes = {
  permisos: PropTypes.array.isRequired,
};


// Se obtienen los datos de los permisos
export default createContainer(() => {

  Meteor.subscribe('permisos');

  return {
    permisos: Permisos.find({}, { sort: { fecha: -1 } }).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, PermisosTabla);
