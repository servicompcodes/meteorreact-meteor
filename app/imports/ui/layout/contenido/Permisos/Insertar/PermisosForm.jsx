import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal, Form, Input, Radio, Select, notification, DatePicker } from 'antd';
const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
const Option = Select.Option;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

export class PermisosForm extends Component {

  render() {


    const tagsDesconocidosDistinct = [...this.props.uidSelect];

    const TagItem = tagsDesconocidosDistinct.map( (tag,index)  => {
        return(
          <Option key={tag._id} value={tag.uid}>{tag.uid}</Option>
        )
    });

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="vertical">
        <FormItem label="TAG(UID)">
          {getFieldDecorator('uid', {
            rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Seleccione un TAG"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {TagItem}
            </Select>
          )}
        </FormItem>
        <FormItem
          label="Rango de Fecha y Hora"
        >
          {getFieldDecorator('range-time-picker', {
            rules: [{ type: 'array', required: true, message: 'Por Favor seleccione Fecha y Hora correspondiente a este permiso' }],
          })(
            <RangePicker showTime format="YYYY-MM-DD HH:mm:ss" />
          )}
        </FormItem>
      </Form>
   );
  }

}

// Se obtienen los datos de los usuarios
export default createContainer(() => {
  Meteor.subscribe('tags');
  Meteor.subscribe('permisos');

  return {
    uidSelect: Tags.find({})
                        .fetch()
                        .filter((tag) => {
                            if( !Permisos.find( {uid:tag.uid} ).fetch().length > 0) {
                              return true;
                            }
                        })
  };
}, PermisosForm);
