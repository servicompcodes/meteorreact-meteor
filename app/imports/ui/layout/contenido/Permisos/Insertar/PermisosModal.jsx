import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Modal, notification, Button, Icon, Tooltip } from 'antd';
import PermisosForm from './PermisosForm.jsx'

export default class Permisos extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false
    };
  }


    showModal = () => {
      this.setState({ visible: true });
    }

   componentWillReceiveProps(nextProps, nextContext){
     this.setState({ visible: nextProps.visible });
   }

   handleCancel = () => {
     this.setState({ visible: false });
   }

     handleCreate = () => {
       const form = this.formRef.props.form;
       form.validateFields((err, values) => {
         if (err) {
           return;
         }

         Meteor.call('permisos.insertar',values.uid,values['range-time-picker'][0].toDate(),values['range-time-picker'][1].toDate());

         notification['success']({
           message: 'Permiso Ingresado',
           description:'Permiso ingresado exitosamente',
           placement:'bottomRight',
         });

         form.resetFields();
         this.setState({ visible: false });
       });
     }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }

  render() {

   const EnhancedForm =  Form.create()(PermisosForm);

    return (<div>
      <Tooltip placement="bottom" title={'Ingresar'}>

      <Button
        size="large"
        type="success"
        onClick={this.showModal}
        >
        <Icon type="plus-circle-o" />
      </Button>
    </Tooltip>

      <Modal
        visible={this.state.visible}
        title="Ingresar Permiso"
        okText="Ingresar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />

      </Modal>

    </div>


   );
  }
}
