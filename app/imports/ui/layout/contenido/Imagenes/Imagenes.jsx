import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import FileUpload from './FileUpload.jsx';

import { Row } from 'antd';

export default class Imagenes extends Component {
  render() {
    return (<Row>
              <h1 style = {{display : "inline"}}><i className="fa fa-image"/> Imagenes </h1>
              <FileUpload/>
            </Row>);
  }
}
