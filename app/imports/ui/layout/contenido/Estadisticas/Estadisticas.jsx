import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import { Row, Col, Card } from 'antd';

import {Animated} from "react-animated-css";

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

export class Estadisticas extends Component {

  render() {
    const Top5MayorTiempoAcumulado = this.props.top5MayorTiempoAcumulado;
    const Top5MenorTiempoAcumulado = this.props.top5MenorTiempoAcumulado;
    const Top5MasVecesAtrasado = this.props.top5MasVecesAtrasado;
    const Top5MenosVecesAtrasado = this.props.top5MenosVecesAtrasado;

    return (
        <Row>
          <h1 style={{display: "inline"}}> <i className="fa fa-line-chart"/> Estadisticas</h1><h3 style={{display: "inline"}}> Mensuales</h3>
          <br/>
          <br/>
          <Row gutter={16}>

            <Col sm={24} md={12}>
              <center>
                <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>

              <Card
                title={<span>Top 5 <b>Trabajadores</b> + Tiempo de Atraso Acumulado</span>}
                bordered={true}
                >
                  {Top5MayorTiempoAcumulado.length>0?
                    <BarChart width={400} height={250} data={Top5MayorTiempoAcumulado}>
                     <CartesianGrid strokeDasharray="3 3"/>
                     <XAxis dataKey="trabajador"/>
                     <YAxis/>
                     <Tooltip/>
                     <Legend />
                     <Bar dataKey="TiempoAcumuladoAtrasos" fill="#45ddca" />
                   </BarChart>:'No Existen Datos'
                  }
               </Card>
             </Animated>

             </center>
            </Col>

             <Col sm={24} md={12}>
               <center>
                 <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>

                 <Card
                   title={<span>Top 5 <b>Trabajadores</b> + Cantidad de Atrasos</span>}
                   bordered={true}
                   >

                   {Top5MasVecesAtrasado.length>0?
                     <BarChart width={400} height={250} data={Top5MasVecesAtrasado}>
                      <CartesianGrid strokeDasharray="3 3"/>
                      <XAxis dataKey="trabajador"/>
                      <YAxis/>
                      <Tooltip/>
                      <Legend />
                      <Bar dataKey="CantidadDeAtrasos" fill="#4ff47b" />
                    </BarChart>:'No Existen Datos'
                   }

                </Card>
              </Animated>

            </center>
            </Col>
          </Row>
          <br></br>
          <Row gutter={16}>
            <Col sm={24} md={12}>
              <center>
                <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>

                <Card
                  title={<span>Top 5 <b>Trabajadores</b> - Tiempo de Atraso Acumulado</span>}
                  bordered={true}
                  >


                  {Top5MenorTiempoAcumulado.length>0?
                    <BarChart width={400} height={250} data={Top5MenorTiempoAcumulado}>
                     <CartesianGrid strokeDasharray="3 3"/>
                     <XAxis dataKey="trabajador"/>
                     <YAxis/>
                     <Tooltip/>
                     <Legend />
                     <Bar dataKey="TiempoAcumuladoAtrasos" fill="#ff304b" />
                   </BarChart>:'No Existen Datos'
                  }

           </Card>
         </Animated>

             </center>
             </Col>
             <Col sm={24} md={12}>
               <center>
                 <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>

                 <Card
                   title={<span>Top 5 <b>Trabajadores</b> - Cantidad de Atrasos</span>}
                   bordered={true}
                   >

                   {Top5MenosVecesAtrasado.length>0?
                     <BarChart width={400} height={250} data={Top5MenosVecesAtrasado}>
                      <CartesianGrid strokeDasharray="3 3"/>
                      <XAxis dataKey="trabajador"/>
                      <YAxis/>
                      <Tooltip/>
                      <Legend />
                      <Bar dataKey="CantidadDeAtrasos" fill="#ff9c32" />
                    </BarChart>:'No Existen Datos'
                   }


          </Card>
        </Animated>


            </center>
            </Col>
          </Row>


        </Row>
    );
  }
}

// Se obtienen los datos de los eventos
export default createContainer(() => {
  Meteor.subscribe('eventos');
  Meteor.subscribe('trabajadores');
  Meteor.subscribe('permisos');


  var ahora = new Date();
  var firstDayOfMonth = new Date(ahora.getFullYear(), ahora.getMonth());
  var lastDayOfMonth = new Date(ahora.getFullYear(), ahora.getMonth() + 1, 0);

  //Obtiene todos los eventos de este mes que corresponden a un trabajador y que tienen atraso (datos de forma granular)
  var eventosTrabajadoresConAtrasosDelMes = Eventos.find({atraso:{$gt: 0 }}).fetch().filter((evento) => {
      if( Trabajadores.find( {uid:evento.uid} ).fetch().length > 0 && evento.fecha>=firstDayOfMonth && evento.fecha<=lastDayOfMonth) {
        return true;
    }
  });

  // Funcion que remueve los duplicados de un array
  function removeDuplicates(a, uid)  {
    return a.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[uid]).indexOf(obj[uid]) === pos;
    });
  }

  // Varable que almacena las UID(TAGS) no duplicadas de los eventos de trabajadores con atrasos
  var uidDistinct = removeDuplicates(eventosTrabajadoresConAtrasosDelMes,'uid');

  var Top5MayorTiempoAcumulado = uidDistinct.map((currentValue, index, array) => {
      return {
        uid:currentValue.uid,
        trabajador:Trabajadores.find( {uid:currentValue.uid} ).fetch()[0].nombre,
        TiempoAcumuladoAtrasos:
          eventosTrabajadoresConAtrasosDelMes.reduce(
            (sumatoria,b) => {
              return currentValue.uid === b.uid?
                      sumatoria+(b.atraso/60):
                      sumatoria
          } ,0)
      }
  }).sort(function(a,b){return b.TiempoAcumuladoAtrasos-a.TiempoAcumuladoAtrasos}).slice(0,5);

  var Top5MenorTiempoAcumulado = uidDistinct.map((currentValue, index, array) => {
      return {
        uid:currentValue.uid,
        trabajador:Trabajadores.find( {uid:currentValue.uid} ).fetch()[0].nombre,
        TiempoAcumuladoAtrasos:
          eventosTrabajadoresConAtrasosDelMes.reduce(
            (sumatoria,b) => {
              return currentValue.uid === b.uid?
                      sumatoria+(b.atraso/60):
                      sumatoria
          } ,0)
      }
  }).sort(function(a,b){return a.TiempoAcumuladoAtrasos-b.TiempoAcumuladoAtrasos}).slice(0,5);

  var Top5MasVecesAtrasado = uidDistinct.map((currentValue, index, array) => {
      return {
        uid:currentValue.uid,
        trabajador:Trabajadores.find( {uid:currentValue.uid} ).fetch()[0].nombre,
        CantidadDeAtrasos: eventosTrabajadoresConAtrasosDelMes.reduce( (contador,b) => { return currentValue.uid === b.uid? contador+1:contador } ,0)}
  }).sort(function(a,b){return b.CantidadDeAtrasos-a.CantidadDeAtrasos}).slice(0,5);


    var Top5MenosVecesAtrasado = uidDistinct.map((currentValue, index, array) => {
        return {
          uid:currentValue.uid,
          trabajador:Trabajadores.find( {uid:currentValue.uid} ).fetch()[0].nombre,
          CantidadDeAtrasos: eventosTrabajadoresConAtrasosDelMes.reduce( (contador,b) => { return currentValue.uid === b.uid? contador+1:contador } ,0)}
    }).sort(function(a,b){return a.CantidadDeAtrasos-b.CantidadDeAtrasos}).slice(0,5);

  return {
    top5MayorTiempoAcumulado: Top5MayorTiempoAcumulado,
    top5MenorTiempoAcumulado: Top5MenorTiempoAcumulado,
    top5MasVecesAtrasado: Top5MasVecesAtrasado,
    top5MenosVecesAtrasado: Top5MenosVecesAtrasado,
  };
}, Estadisticas);
