import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Input, Select } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;

import { createContainer } from 'meteor/react-meteor-data';
import InputMask from 'react-input-mask';

export class VisitasForm extends Component {

  state = {
    uploading: [],
    progress: 0,
    inProgress: false,
    cargado:false,
    url:'',
  }

  handleChange(e) {


  }

  render() {

    const eventosDesconocidosDistinct = [...this.props.uidSelect];

    const eventosTagDesconocidoItem = eventosDesconocidosDistinct.map( (evento)  => {
        return(
          <Option key={evento._id} value={evento.uid}>{evento.uid}</Option>
        )
    });


    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="vertical">

        <FormItem label="UID">
          {getFieldDecorator('uid', {
            rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Seleccione un TAG"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {eventosTagDesconocidoItem}
            </Select>
          )}
          </FormItem>

        <FormItem label="RUT">
          {getFieldDecorator('rut', {
            rules: [{ required: true, message: 'Por favor ingrese un rut' }],
          })(
            <InputMask
              className="ant-input"
              alwaysShowMask={true}
              mask="99.999.999-*" />

          )}
        </FormItem>
        <FormItem label="Nombre">
          {getFieldDecorator('nombre', {
            rules: [{ required: true, message: 'Por favor el nombre' }],
          })(
            <Input />
          )}
        </FormItem>
      </Form>
   );
  }

}


// Se obtienen los datos de los usuarios
export default createContainer(() => {
  Meteor.subscribe('automoviles.all');
  Meteor.subscribe('trabajadores');
  Meteor.subscribe('visitas');
  Meteor.subscribe('tags');

  return {
    uidSelect: Tags.find({})
                        .fetch()
                        .filter((tag) => {
                            if( !Automoviles.find( {uid:tag.uid} ).fetch().length > 0 && !Trabajadores.find( {uid:tag.uid} ).fetch().length > 0 && !Visitas.find( {uid:tag.uid} ).fetch().length > 0) {
                              return true;
                            }
                        })
  };
}, VisitasForm);
