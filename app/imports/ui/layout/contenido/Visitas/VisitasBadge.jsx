import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Badge, Tooltip } from 'antd';

export class VisitasBadge extends React.Component {
  render() {
    return (
      <Tooltip placement="bottom" title="Visitas">
        <Link to='/visitas'>
          <Badge count={this.props.visitas}>
              <span>
                  <i className="fa fa-address-card-o"/>
              </span>
          </Badge>
        </Link>
      </Tooltip>
    );
  }
}

export default createContainer(() => {

  //Se subscribe a la coleccion de visitas
  Meteor.subscribe('visitas');

  /*Dentro del objeto return, todos los elementos definidos se convierten en propiedades
    del componente React */
  return {
    visitas: Visitas.find({}, { sort: { fecha: -1 } }).count(),
  };
}, VisitasBadge);
