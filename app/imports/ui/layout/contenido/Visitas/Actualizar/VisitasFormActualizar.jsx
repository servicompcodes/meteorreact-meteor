import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Input, Select } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;

import { createContainer } from 'meteor/react-meteor-data';

export default class VisitasFormActualizar extends Component {

  state = {
    uploading: [],
    progress: 0,
    inProgress: false,
    cargado:false,
    url:'',
  }

  render() {


    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="vertical">

        <FormItem label="UID">
          {getFieldDecorator('uid', {
            rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
          })(
            <Select
              disabled
              mode="multiple"
              placeholder="Seleccione un TAG"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >

            </Select>
          )}
          </FormItem>

        <FormItem label="RUT">
          {getFieldDecorator('rut', {
            rules: [{ required: true, message: 'Por favor ingrese un rut' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem label="Nombre">
          {getFieldDecorator('nombre', {
            rules: [{ required: true, message: 'Por favor el nombre' }],
          })(
            <Input />
          )}
        </FormItem>
      </Form>
   );
  }

}
