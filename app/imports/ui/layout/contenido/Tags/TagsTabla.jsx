import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Row, Col, Affix, Tooltip, Table, Button, Icon, Switch, Radio, Form, notification } from 'antd';
const FormItem = Form.Item;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

import TagsModal from './TagsModal.jsx';


const columns = [{
  title: 'UID',
  dataIndex: 'uid',
}];

export class TagsTabla extends Component {

  constructor(props){
    super(props);
    this.state = {
      filasSeleccionadas: [],  // Check here to configure the default column
      cargando: false
    };
  }


  eliminarSeleccionados = () => {

    //Setea el estado de cargando a la tabla
    this.setState({ cargando: true});
    // ajax request after empty completing
    Meteor.call('tags.eliminarSeleccionados',this.state.filasSeleccionadas);

    notification.open({
      message: 'Tags Eliminados',
      description:'Tags eliminados exitosamente',
      placement:'bottomRight',
      icon: <Icon type="delete" style={{ color: 'red' }} />,
    });

    this.setState({
      filasSeleccionadas: [],
      cargando: false
    });

  }

  onSelectChange = (filasSeleccionadas) => {
    this.setState({ filasSeleccionadas });
  }

  /*
    rowKey="_id"
    Esta propiedad dentro de table permite definiar una columna
    la cual será la "key" que necesita el componente para identificar
    los elementos seleccionados.
  */

  render() {

      const { cargando, filasSeleccionadas } = this.state;

    const rowSelection = {
      filasSeleccionadas,
      onChange: this.onSelectChange,
    };

    const haySeleccionados = filasSeleccionadas.length > 0;

    return (
      <div>

        <Row>
          <Col span={12}>
            <h1 style = {{display : "inline"}}><i className="fa fa-address-card"/> Tags </h1>

            <span >
              {haySeleccionados ? ' - '+filasSeleccionadas.length+' Elemento(s) seleccionado(s)' : ''}
            </span>

          </Col>


          <Col span={12}>
            <Affix offsetTop={12}>
              <Row type="flex" justify="end" gutter={8} >
                <Col >
                      <TagsModal/>
                </Col>
                  <Col >
                    <Tooltip placement="bottom" title={'Eliminar'}>
                     <Button
                       type="danger"
                       size="large"
                       onClick={this.eliminarSeleccionados}
                       disabled={!haySeleccionados}
                       loading={cargando}
                     >
                       <Icon type="minus-circle-o" />
                     </Button>
                   </Tooltip>
                  </Col>
                </Row>
              </Affix>
          </Col>

      </Row>
      <br>
      </br>
      <Row>
        <Table
          rowKey="_id"
          rowSelection={rowSelection}
          columns={columns}
          dataSource={this.props.tags} />
        </Row>
      </div>
    );
  }
}

// Se definen los datos requeridos
TagsTabla.propTypes = {
  tags: PropTypes.array.isRequired,
};


// Se obtienen los datos de los tags
export default createContainer(() => {

  Meteor.subscribe('tags');

  return {
    tags: Tags.find({}, { sort: { fecha: -1 } }).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, TagsTabla);
