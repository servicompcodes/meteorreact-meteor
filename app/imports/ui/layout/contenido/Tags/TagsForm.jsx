import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal, Form, Input, Radio, Select, notification, Upload, Icon } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

export class TagsForm extends Component {

  render() {

    const eventosDesconocidosDistinct = [...this.props.tagsSelect];

    const eventosTagDesconocidoItem = eventosDesconocidosDistinct.map( (evento)  => {
        return(
          <Option key={evento._id} value={evento.uid}>{evento.uid}</Option>
        )
    });

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="vertical">
        <FormItem label="UID">
          {getFieldDecorator('uid', {
            rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Seleccione un TAG"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {eventosTagDesconocidoItem}
            </Select>
          )}
          </FormItem>
      </Form>
   );
  }

}

// Se obtienen los datos de los usuarios
export default createContainer(() => {
  Meteor.subscribe('eventos');
  Meteor.subscribe('tags');

  return {
    tagsSelect: Eventos.find({tipo:'Tag Desconocido'})
                        .fetch()
                        .filter((evento) => {
                            if( !Tags.find( {uid:evento.uid} ).fetch().length > 0 ) {
                              return true;
                            }
                        })
  };
}, TagsForm);
