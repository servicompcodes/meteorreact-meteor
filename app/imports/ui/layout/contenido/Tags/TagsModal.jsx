import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Modal, notification, Button, Icon, Tooltip } from 'antd';
import TagsForm from './TagsForm.jsx'

export default class TagsModal extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  }

  componentWillReceiveProps(nextProps, nextContext){
    this.setState({ visible: nextProps.visible });
  }

   handleCancel = () => {
     this.setState({ visible: false });
   }

   handleCreate = () => {
     const form = this.formRef.props.form;
     form.validateFields((err, values) => {
       if (err) {
         return;
       }
       console.log('Received values of form: ', values);

       Meteor.call('tags.insertar',values.uid);

       notification['success']({
         message: 'Tag Ingresado',
         description:'Tag ingresado exitosamente',
         placement:'bottomRight',
       });

       form.resetFields();
     });

     this.setState({ visible: false });

   }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }

  render() {

   const EnhancedForm =  Form.create()(TagsForm);

    return (
      <div>
        <Tooltip placement="bottom" title={'Ingresar'}>
        <Button
          size="large"
          type="success"
          onClick={this.showModal}
          >
          <Icon type="plus-circle-o" />
        </Button>
      </Tooltip>

      <Modal
        visible={this.state.visible}
        title="Ingresar Tag"
        okText="Ingresar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />

      </Modal>
      </div>

   );
  }
}
