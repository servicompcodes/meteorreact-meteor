import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Table, Button, Icon, Switch, Radio, Form, notification, Row, Affix, Tooltip, Col } from 'antd';
const FormItem = Form.Item;

// "Middleware" entre meteor y react
import { createContainer } from 'meteor/react-meteor-data';

import AutomovilesModal from './Insertar/AutomovilesModal.jsx';
import AutomovilesModalActualizar from './Actualizar/AutomovilesModalActualizar.jsx';

const columns = [{
  title: 'UID',
  dataIndex: 'uid',
}, {
  title: 'Patente',
  dataIndex: 'patente',
}, {
  title: 'Marca',
  dataIndex: 'marca',
}, {
  title: 'Modelo',
  dataIndex: 'modelo',
}, {
  title: 'Color',
  dataIndex: 'color',
}];

export class AutomovilesTabla extends Component {

  state = {
    filasSeleccionadas: [],  // Check here to configure the default column
    cargando: false
  };

  eliminarSeleccionados = () => {

    //Setea el estado de cargando a la tabla
    this.setState({ cargando: true });
    // ajax request after empty completing
    Meteor.call('automoviles.eliminarSeleccionados',this.state.filasSeleccionadas);

    notification.open({
      message: 'Automoviles Eliminados',
      description:'Automoviles eliminados exitosamente',
      placement:'bottomRight',
      icon: <Icon type="delete" style={{ color: 'red' }} />,
    });

    this.setState({
      filasSeleccionadas: [],
      cargando: false,
    });

  }

  start = () => {
    this.setState({ cargando: true });
    // ajax request after empty completing

    setTimeout(() => {
      this.setState({
        filasSeleccionadas: [],
        cargando: false,
      });
    }, 1000);
  }

  onSelectChange = (filasSeleccionadas) => {
    this.setState({ filasSeleccionadas });
  }


  /*
    rowKey="_id"
    Esta propiedad dentro de table permite definiar una columna
    la cual será la "key" que necesita el componente para identificar
    los elementos seleccionados.
  */

  render() {

      const { cargando, filasSeleccionadas } = this.state;

    const rowSelection = {
      filasSeleccionadas,
      onChange: this.onSelectChange,
    };

    const haySeleccionados = filasSeleccionadas.length > 0;

    return (
      <div>
        <Row>
          <Col span={12}>
            <h1 style = {{display : "inline"}}><i className="fa fa-car"/> Automoviles </h1>
            <span >
              {haySeleccionados ? ' - '+filasSeleccionadas.length+' Elemento(s) seleccionado(s)' : ''}
            </span>
          </Col>


          <Col span={12}>
            <Affix offsetTop={12}>
              <Row type="flex" justify="end" gutter={8} >
                <Col >
                  <AutomovilesModal></AutomovilesModal>
                </Col>
                <Col >
                  <AutomovilesModalActualizar
                    haySeleccionados={haySeleccionados}
                    uidSelect={rowSelection.filasSeleccionadas}
                    />
                </Col>
                  <Col >
                    <Tooltip placement="bottom" title={'Eliminar'}>
                     <Button
                       type="danger"
                       size="large"
                       onClick={this.eliminarSeleccionados}
                       disabled={!haySeleccionados}
                       loading={cargando}
                     >
                       <Icon type="minus-circle-o" />
                     </Button>
                   </Tooltip>
                  </Col>
                </Row>
              </Affix>
          </Col>

      </Row>
      <br>
      </br>
      <Row>
        <Table
          rowKey="_id"
          rowSelection={rowSelection}
          columns={columns}
          dataSource={this.props.automoviles} />
        </Row>

      </div>
    );
  }
}

// Se definen los datos requeridos
AutomovilesTabla.propTypes = {
  automoviles: PropTypes.array.isRequired,
};

// Se obtienen los datos de los automoviles
export default createContainer(() => {
  Meteor.subscribe('automoviles.all');

  return {
    automoviles: Automoviles.find({}, { sort: { fecha: -1 } }).fetch(),
    usuarioActual: Meteor.user(),
    nombreUsuarioActual: Meteor.user()? Meteor.user().username:'',
  };
}, AutomovilesTabla);
