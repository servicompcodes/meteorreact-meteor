import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { createContainer } from 'meteor/react-meteor-data';

import { Button, Modal, Form, Input, Radio, Select, notification, Upload, Icon } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

import InputMask from 'react-input-mask';

function formatearPatente(value) {
  var res = value.split("").splice(2, 0,"-");
  return value;
}

export class AutomovilesForm extends Component {


    constructor(props){
      super(props);
      this.state = {
        patente: ''
      };
    }

    normalizarPatente = (value, prevValue = []) => {

      const reg = /^[a-zA-Z][a-zA-Z]?[-]?([0-9]|[a-zA-Z])?([0-9]|[a-zA-Z])?[-]?[0-9]?[0-9]?$/;
      if (reg.test(value)) {
        return formatearPatente(value);
      }
    }


  render() {

    const eventosDesconocidosDistinct = [...this.props.uidSelect];

    const eventosTagDesconocidoItem = eventosDesconocidosDistinct.map( (evento)  => {
        return(
          <Option key={evento._id} value={evento.uid}>{evento.uid}</Option>
        )
    });

    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (

              <Form layout="vertical">
                <FormItem label="UID">
                  {getFieldDecorator('uid', {
                    rules: [{ required: true, message: 'Por favor escanee el TAG RFID' }],
                  })(
                    <Select
                      showSearch
                      style={{ width: 200 }}
                      placeholder="Seleccione un TAG"
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {eventosTagDesconocidoItem}
                    </Select>
                  )}
                  </FormItem>
                <FormItem label="Patente">
                  {getFieldDecorator('patente', {
                    rules: [{ required: true, message: 'Por favor ingrese una patente' }],
                    normalize: this.normalizarPatente
                  })(
                      <InputMask
                        className="ant-input"
                        alwaysShowMask={true}
                        mask="aa-**-99"
                        />

                  )}
                </FormItem>
                <FormItem label="Marca">
                  {getFieldDecorator('marca', {
                    rules: [{ required: true, message: 'Por favor seleccione una marca' }],
                  })(
                    <Select
                      showSearch
                      style={{ width: 200 }}
                      placeholder="Seleccione una marca"
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      <Option value="Nissan">Nissan</Option>
                      <Option value="Hyundai">Hyundai</Option>
                      <Option value="BMW">BMW</Option>
                    </Select>
                  )}
                </FormItem>
                <FormItem label="Modelo">
                  {getFieldDecorator('modelo', {
                    rules: [{ required: true, message: 'Por favor el nombre' }],
                  })(
                    <Select
                      showSearch
                      style={{ width: 200 }}
                      placeholder="Seleccione un modelo"
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      <Option value="Terracan">Terracan</Option>
                      <Option value="Lancer">Lancer</Option>
                      <Option value="Impresa">Impresa</Option>
                    </Select>
                  )}
                </FormItem>
                <FormItem label="Color">
                  {getFieldDecorator('color', {
                    rules: [{ required: true, message: 'Por favor ingrese un color' }],
                  })(
                    <Select
                      showSearch
                      style={{ width: 200 }}
                      placeholder="Seleccione un color"
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      <Option value="Rojo">Rojo</Option>
                      <Option value="Naranjo">Naranjo</Option>
                      <Option value="Azul">Azul</Option>
                    </Select>
                  )}
                </FormItem>
              </Form>
   );
  }

}


// Se obtienen los datos de los usuarios
export default createContainer(() => {
  Meteor.subscribe('automoviles.all');
  Meteor.subscribe('trabajadores');
  Meteor.subscribe('visitas');
  Meteor.subscribe('tags');

  return {
    uidSelect: Tags.find({})
                        .fetch()
                        .filter((tag) => {
                            if( !Automoviles.find( {uid:tag.uid} ).fetch().length > 0 && !Trabajadores.find( {uid:tag.uid} ).fetch().length > 0 && !Visitas.find( {uid:tag.uid} ).fetch().length > 0) {
                              return true;
                            }
                        })
  };
}, AutomovilesForm);
