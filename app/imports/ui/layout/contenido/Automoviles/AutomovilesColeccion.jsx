import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import AutomovilesTabla from './AutomovilesTabla.jsx';

export default class AutomovilesColeccion extends Component {
  render() {
    return (<AutomovilesTabla/>);
  }
}
