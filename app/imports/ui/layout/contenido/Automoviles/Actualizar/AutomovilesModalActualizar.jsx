import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Form, Modal, notification, Tooltip, Button, Icon } from 'antd';
import AutomovilesFormActualizar from './AutomovilesFormActualizar.jsx'

import { createContainer } from 'meteor/react-meteor-data';

export class AutomovilesModalActualizar extends Component {

  constructor(props){
    super(props);
    this.state = {
      visible: false,
      loading: false
    };
  }
  
  showModal = () => {
    this.setState({ visible: true });
  }

      componentDidUpdate(prevProps, prevState, snapshot){

          var uids = this.props.automoviles
            .filter( (automovil) => {
                return this.props.uidSelect.find( (id) =>
                        {	return id==automovil._id }
                        )?true:false;
            })
            .map( (automovil) => {
              return this.props.uidSelect.find( (id) =>
                          {	return id==automovil._id }
                        )?automovil.uid:'';
            });

          this.formRef?this.formRef.props.form.setFieldsValue({uid:uids}):'';

      }

   handleCancel = () => {
     this.setState({ visible: false });
   }

     handleCreate = () => {
       const form = this.formRef.props.form;
       form.validateFields((err, values) => {
         if (err) {
           return;
         } else {

           Meteor.call('automoviles.insertar',values.uid,values.patente,values.marca,values.modelo,values.color);

             notification['success']({
             message: 'Automovil Ingresado',
             description:'Automovil ingresado exitosamente',
             placement:'bottomRight',
           });



           form.resetFields();
           this.setState({ visible: false });


         }


       });
     }

   saveFormRef = (formRef) => {
      this.formRef = formRef;
    }

    uploadIt = (e) => {
      e.preventDefault();


      if (e.currentTarget.files && e.currentTarget.files[0]) {
        // We upload only one file, in case
        // there was multiple files selected
        var file = e.currentTarget.files[0];

        this.setState({file:file});


      }
    }

  render() {

   const EnhancedForm =  Form.create()(AutomovilesFormActualizar);

    return (<div>

      <Tooltip placement="bottom" title={'Actualizar'}>
        <Button
          type="primary"
          size="large"
          disabled={!this.props.haySeleccionados}
          onClick={this.showModal}
        >
          <Icon type="sync" />
        </Button>
      </Tooltip>

      <Modal
        visible={this.state.visible}
        title="Ingresar Automovil"
        okText="Ingresar"
        onCancel={this.handleCancel}
        onOk={this.handleCreate}
      >
      <h4>Imagen:</h4>
      <input
        type="file"
        id="fileinput"
        disabled={this.state.inProgress}
        ref="fileinput"
        onChange={this.uploadIt}
      /><br/>
        <EnhancedForm wrappedComponentRef={this.saveFormRef} />
      </Modal>

          </div>
   );
  }
}


// Se obtienen los datos de los automoviles
export default createContainer(() => {
  Meteor.subscribe('automoviles');
  return {
    automoviles: Automoviles.find({}, { sort: { fecha: -1 } }).fetch()
  };
}, AutomovilesModalActualizar);
