import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route } from 'react-router-dom';

import { createContainer } from 'meteor/react-meteor-data';

//Lenguaje Español Ant-Design
import { LocaleProvider } from 'antd';
import esEs from 'antd/lib/locale-provider/es_ES';

//Componentes de layout de ant-design
import { Layout, Row } from 'antd';
const { Header, Sider, Content, Footer } = Layout;

import SidebarMenu from './layout/SidebarMenu.jsx';
import NavbarMenu from './layout/NavbarMenu.jsx';
import Contenido from './layout/Contenido.jsx';
import Notificaciones from './layout/Notificaciones.jsx';

import IniciarSesion from './layout/contenido/Usuarios/IniciarSesion.jsx';
import NotFound from './layout/contenido/NotFound.jsx';

// App component - represents the whole app
export class App extends Component {

  //Propiedades del Slider
  state = {
    collapsed: true,
  };
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };


  render() {

    var loggedInOrNotContent;

    if (this.props.usuarioActual) {
      loggedInOrNotContent =
      <div>
      {
        this.props.usuarioActual? <Notificaciones/>:''
      }
        <Switch>
          <LocaleProvider locale={esEs}>
            <Layout style={{height:"100vh"}}>
              {
                this.props.usuarioActual?
                  <Sider
                  width="250"
                  breakpoint="md"
                  collapsedWidth="0"
                  >
                    <center>
                      <img src="/images/logo.png"></img>
                    </center>
                      <Route path='/' component={SidebarMenu} />

                  </Sider>:''
              }

              <Layout>
                {
                  this.props.usuarioActual?
                  <Header style={{ background: '#fff' }}>
                      <Route path='/' component={NavbarMenu} />

                  </Header>:''
                }

                <Content style={{ margin: '24px 16px', padding: 24, background: '#fff'}}>
                    <Route path='/' component={Contenido} />
                </Content>

                <Footer style={{ textAlign: 'center' }}>
                    WebApp Reactiva diseñada y desarrollada por <a href="https://www.basthianmatthews.cl">mortSoudain</a>
                </Footer>
              </Layout>

            </Layout>
          </LocaleProvider>
        </Switch>
      </div>;
    } else {
      loggedInOrNotContent =
        <Switch>
          <Route exact path='/' component={IniciarSesion} />
          <Route component={NotFound} />
        </Switch>;
    }



    return (<div>{loggedInOrNotContent}</div>


     );
  }
}

export default createContainer(() => {
  return {
    usuarioActual: Meteor.user(),
  };
}, App);
