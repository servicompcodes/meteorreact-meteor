import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

//Componentes para enrutar aplicacion
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from '../../ui/layout/contenido/Home.jsx';
import MiPerfil from '../../ui/layout/contenido/Usuarios/MiPerfil.jsx';
import AppConfig from '../../ui/layout/contenido/App/AppConfig.jsx';
import ComponentesProbados from '../../ui/layout/contenido/ComponentesProbados/ComponentesProbados.jsx';
import UsuariosColeccion from '../../ui/layout/contenido/Usuarios/UsuariosColeccion.jsx';
import EventosColeccion from '../../ui/layout/contenido/Eventos/EventosColeccion.jsx';
import TagsColeccion from '../../ui/layout/contenido/Tags/TagsColeccion.jsx';
import PermisosColeccion from '../../ui/layout/contenido/Permisos/PermisosColeccion.jsx';
import UltimoEvento from '../../ui/layout/contenido/Eventos/UltimoEvento.jsx';
import TrabajadoresColeccion from '../../ui/layout/contenido/Trabajadores/TrabajadoresColeccion.jsx';
import VisitasColeccion from '../../ui/layout/contenido/Visitas/VisitasColeccion.jsx';
import AutomovilesColeccion from '../../ui/layout/contenido/Automoviles/AutomovilesColeccion.jsx';
import ArduinosColeccion from '../../ui/layout/contenido/Arduinos/ArduinosColeccion.jsx';
import Estadisticas from '../../ui/layout/contenido/Estadisticas/Estadisticas.jsx';
import Imagenes from '../../ui/layout/contenido/Imagenes/Imagenes.jsx';

import App from '../../ui/App.jsx';

export var rutas = [
    {exact:true, path:'/' , component:Home},
    {path:'/miperfil' , component:MiPerfil},
    {path:'/componentesprobados' , component:ComponentesProbados},
    {path:'/usuarios' , component:UsuariosColeccion},
    {path:'/eventos' , component:EventosColeccion},
    {path:'/tags' , component:TagsColeccion},
    {path:'/permisos' , component:PermisosColeccion},
    {path:'/ultimoEvento' , component:UltimoEvento},
    {path:'/trabajadores' , component:TrabajadoresColeccion},
    {path:'/visitas' , component:VisitasColeccion},
    {path:'/automoviles' , component:AutomovilesColeccion},
    {path:'/arduinos' , component:ArduinosColeccion},
    {path:'/estadisticas' , component:Estadisticas},
    {path:'/imagenes' , component:Imagenes},
    {path:'/appconfig' , component:AppConfig}
];

export default class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
          <Switch>
            <Route path='/' component={App} />
          </Switch>
      </BrowserRouter>
    );
  }
}
