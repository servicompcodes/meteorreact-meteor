import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { render } from 'react-dom';

import './accounts-config.js';
import Routes from './Routes.jsx';

Meteor.startup( () => {
  render(<Routes/>,document.getElementById('render-target'));
});