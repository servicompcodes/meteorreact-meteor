// This defines a starting set of data to be loaded if the app is loaded with an empty db.
//import './fixtures.js';

// This defines all the collections, publications and methods that the application provides
// as an API to the client.
//import '../../api/api.js';

import SerialPort from 'serialport';
import moment from 'moment';


Meteor.methods({
  'arduinos.leer'() {

  // Cada 30 segundos...
  var ReaderRFIDTimmer = Meteor.setInterval(function() {

    // Leer puertos COM
    SerialPort.list(Meteor.bindEnvironment(function (err, ports) {

      // Eliminar registros antiguos (esta coleccion solo almacena conecciones activas)
      Arduinos.remove({});

      // Por cada puerto abierto encontrado
      ports.forEach(function(port) {

        // Insertar informacion del arduino (puerto COM)
          Arduinos.insert({
            productoID:port.productId,
            vendedorID:port.vendorId,
            fabricante: port.manufacturer,
            serial: port.serialNumber,
            puerto: port.comName,
          });


          // Almacena el nunmero del puerto, y configura su inicialozacion
          //TODO: Verificar que el puerto no esté abierto ya...
          var puerto = new SerialPort(port.comName, {
            parser: SerialPort.parsers.readline('\n')
          });

          // Open errors will be emitted as an error event
          puerto.on('error', function(err) {
            console.log('Error: ', err.message)
          })

          //Si el puerto se abre, imprime port open
          puerto.on('open', () => console.log('Port open'));

          // Al recibir datos...
          puerto.on('data', Meteor.bindEnvironment(function(data) {

            var arduino = data.substring(0, 8);
            var uid = data.substring(10, 21);
            var tipo;
            var estado;
            var fecha = new Date();
            var atraso;
            //var atraso;

            ///////ASIGNACION DE TIPO////////////////
              //Busca UID dentro de tags registrados, si encuentra
              if(Tags.findOne({uid:uid.trim()})){

                if(! (Automoviles.findOne({uid:uid}) || Trabajadores.findOne({uid:uid}) || Visitas.findOne({uid:uid})) ){
                  tipo = 'Tag Registrado';
                } else {

                  //Busca ultimo evento asociado a UID
                  if(Eventos.findOne({uid:uid})){
                    var tipoEventoAnterior = Eventos.findOne
                    ({
                      uid:uid
                    },
                    {
                      sort  : {fecha: -1},
                      limit : 1
                    }).tipo;

                    //Si el último evento es de tipo entrada,
                    //el nuevo evento es una salida y vice-versa
                      if(tipoEventoAnterior === 'Entrada'){
                        tipo='Salida';
                      } else tipo='Entrada';
                  }
                  //Si no existe evento asociado, el tipo es una entrada
                  else {
                    tipo = 'Entrada';
                  }

                }

              }
              //Si la UID no está registrada, es un TAG de tipo desconocido
              else {
                tipo = 'Tag Desconocido';
              }

           ///////ASIGNACION DE ESTADO////////////////
             if( Permisos.findOne({ uid:uid.trim() }) ) {

               var fechaMoment = moment(fecha);

               var permiso = Permisos.findOne({ uid:uid.trim() });

               var fechayhoraIni = moment(permiso.fechayhoraIni);
               var fechayhoraFin = moment(permiso.fechayhoraFin);

               if(fechaMoment.isBetween(fechayhoraIni,fechayhoraFin, 'day')){
                 var fechaHora = moment(fechaMoment,'hh:mm:ss');
                 var horaIni = moment(fechayhoraIni,'hh:mm:ss');
                 var horaFin = moment(fechayhoraFin,'hh:mm:ss');

                 if( fechaHora.isBetween(horaIni,horaFin) ){
                   estado = 'Autorizado';
                 }
               } else {
                 estado = 'No Autorizado';
               }

             } else {
               estado = 'Sin Permiso Asignado';
             }

            ///////ASIGNACION DE ATRASO////////////////

                var firstHourDay = new Date(fecha.getFullYear(), fecha.getMonth(),fecha.getDate());


                  /// REVISAR ESTA BUSQUEDA, PUES SI SE REALIZA TAL Y COMO ESTÁ, NO SE VA A PARAR DE REALIZAR EL CALCULO HASTA QUE EXISTA ALGUN ATRASO, LO QUE PLANTEA
                  /// QUE SI LA PERSONA LLEGASE A ENTRAR ANTES O JUSTO AL MOMENTO Y SETEARSE EN 0 EL ATRASO, SE PARCARIA LA PRIMERA ENTRADA CON ATRASO AL "ALMUERZO"

                  // Busco si hay algun evento de tipo entrada y que tenga la misma UID del evento actual,
                  // entre las 00:00 y la hora del evento actual
                var entradasMismoDiaMismaUID = Eventos.find({uid:uid.trim(),fecha:{$gte: firstHourDay,$lte:fecha },tipo:'Entrada'}).fetch();


                if(Permisos.findOne({ uid:uid.trim() })){
                  var permiso = Permisos.findOne({ uid:uid.trim() }).fechayhoraIni;

                  var fechayhoraIni = moment(new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate(), permiso.getHours() , permiso.getMinutes() , permiso.getSeconds()) );

                  // Si no hay ninguna entrada, y ésta es una entrada
                  if(tipo=='Entrada' && !entradasMismoDiaMismaUID.length>0){
                    atraso = moment(fecha).diff(fechayhoraIni, 'minutes')<0?0:moment(fecha).diff(fechayhoraIni, 'minutes');
                  }else {
                    atraso = 0;
                  }
                } else {
                  atraso = 0;
                }

            Meteor.call('eventos.insertar',uid,tipo,estado,arduino,atraso,fecha);
          }, function(error) {
            throw error;
          }));


      });
    }));

    return Arduinos.find().count();

  },10000);

  }

});

if(Meteor.isServer){
  Meteor.call('arduinos.leer');
}
