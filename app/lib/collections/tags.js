import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Tags = new Mongo.Collection('tags');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('tags', function tagsPublicacion() {
    return Tags.find();
  });
}

Meteor.methods({

  'tags.insertar'(uid) {
    check(uid, String);

    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Tags.insert({
      uid,
      propietario: Meteor.userId(),
      solicitante: Meteor.user().username,
    });
  },
      'tags.eliminarTodos'() {
        if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
          throw new Meteor.Error('not-authorized');
        }

        Tags.remove({});
      },

  'tags.setearLeida'(tagId, setearLeida) {

    check(tagId, String);
    check(setearLeida, Boolean);

    const tag = Tags.findOne(tagId);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      // If the task is private, make sure only the owner can check it off
      throw new Meteor.Error('not-authorized');

    }

    Tags.update(tagId, { $set: { leida: setearLeida } });

  },

  'tags.eliminarSeleccionados'(seleccionados) {
    check(seleccionados, [String]);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {
      Tags.remove({_id:seleccionado});
    });

  },

});
