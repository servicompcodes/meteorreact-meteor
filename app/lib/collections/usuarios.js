import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

if (Meteor.isServer) {
  Meteor.publish("usuarios", function(){
    return Meteor.users.find({});
  });
}

Meteor.methods({

  'usuarios.registrar'(username,password,email) {
    Accounts.createUser({
        username: username,
        email: email,
        password: password
    });
  },

  'usuarios.eliminarSeleccionados'(seleccionados) {
    check(seleccionados, [String]);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {
      Meteor.users.remove(seleccionado);
    });

  },

});
