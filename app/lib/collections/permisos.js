import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';


Permisos = new Mongo.Collection('permisos');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('permisos', function permisosPublicacion() {
    return Permisos.find();
  });
}

Meteor.methods({
  'permisos.insertar'(uid,fechayhoraInicio,fechayHoraTermino) {

    var fechayhoraIni = new Date(fechayhoraInicio);
    var fechayhoraFin = new Date(fechayHoraTermino);

    check(uid, String);

    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Permisos.insert({
      uid,
      fechayhoraIni,
      fechayhoraFin
    });
  },

  'permisos.eliminarSeleccionados'(seleccionados) {

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {
      Permisos.remove({_id:seleccionado});
    });

  },
  'permisos.actualizar'(uid,fechayhoraIni,fechayhoraFin) {

    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    uid.map( (seleccionado) => {
      var permiso = Permisos.findOne({uid:seleccionado});
      Permisos.update(permiso._id,{ $set: { fechayhoraIni : fechayhoraIni, fechayhoraFin : fechayhoraFin } } );
    });

  }

});
