import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Eventos = new Mongo.Collection('eventos');

async function hola () {
  const result = await Eventos.rawCollection().distinct().fetch();
  return result;
}

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('eventos', function eventosPublicacion() {
    return Eventos.find();
  });
/*
  Meteor.publish('eventos.tag.desconocido', function eventosPublicacion() {
    const hola = hola();

    return hola;
  });
  */
}
/*
const distinctifiedId = filterSomehow(

  var destArray = _.uniq(sourceArray, function(x){
      return x.name;
  });

  return Collection.find({ _id: {$in: distinctifiedId }});

);
*/
Meteor.methods({

  'eventos.insertar'(uid,tipo,estado,arduino,atraso,fecha) {
/*
    var tipoPropietario;

    if(Automoviles.findOne({uid:uid})){
      tipoPropietario = 'Automovil';
    } else if(Trabajadores.findOne({uid:uid})){
      tipoPropietario = 'Trabajador';
    } else if(Visitas.findOne({uid:uid})){
      tipoPropietario = 'Visita';
    } else {
      tipoPropietario = 'Tag Desconocido';
    }
*/
    Eventos.insert({
      uid,
      tipo,
      fecha,
      estado,
      atraso,
      arduino,
    });
  },

  'eventos.eliminarTodos'() {
    Eventos.remove({});
  },

  'eventos.eliminarSeleccionados'(seleccionados) {
    console.log(seleccionados);
    check(seleccionados, [String]);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {
      Eventos.remove({_id:seleccionado});
    });

  },



});
