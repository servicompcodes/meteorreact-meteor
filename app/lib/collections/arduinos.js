import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Arduinos = new Mongo.Collection('arduinos');

if (Meteor.isServer) {

  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('arduinos', function arduinosPublicacion() {
    return Arduinos.find();
  });
}
