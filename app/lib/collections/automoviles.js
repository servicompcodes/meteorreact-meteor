import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Automoviles = new Mongo.Collection('automoviles');

if (Meteor.isServer) {
  Meteor.publish('automoviles.all', () => {return Automoviles.find()});
}

Meteor.methods({
  'automoviles.insertar'(uid,patente,marca,modelo,color) {
    check(uid, String);
    check(patente, String);
    check(marca, String);
    check(modelo, String);
    check(color, String);

    // Make sure the user is logged in before inserting a task
    Automoviles.insert({
      uid,
      patente,
      marca,
      modelo,
      color,
      fecha: new Date()
    });
  },
  'automoviles.eliminarTodos'() {
      if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
        throw new Meteor.Error('not-authorized');
      }

      Automoviles.remove({});
    },
  'automoviles.eliminarSeleccionados'(seleccionados) {
    check(seleccionados, [String]);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {
      // Obtiene uid del automovil a eliminar para obtener sus eventos de Entrada y Salida
      var uid = Automoviles.findOne({_id:seleccionado}).uid;
      var events = Eventos.find({uid:uid,
          "$or": [{
              "tipo": "Entrada"
          }, {
              "tipo": "Salida"
          }]
      }).fetch().map((sel)=>{return sel._id});

      // Se eliminan automovil y sus eventos
      Meteor.call('eventos.eliminarSeleccionados',events);
      Meteor.call('imagenes.eliminar',uid);
      Automoviles.remove({_id:seleccionado});
    });

  },
});
