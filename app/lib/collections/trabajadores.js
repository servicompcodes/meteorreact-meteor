import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Trabajadores = new Mongo.Collection('trabajadores');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('trabajadores', function trabajadoresPublicacion() {
    return Trabajadores.find();
  });
}

Meteor.methods({

  'trabajadores.insertar'(uid,rut,cargo,nombre,apellidop,apellidom) {
    check(uid, String);
    check(rut, String);
    check(cargo, String);
    check(nombre, String);
    check(apellidop, String);
    check(apellidom, String);

    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Trabajadores.insert({
      uid,
      rut,
      cargo,
      nombre,
      apellidop,
      apellidom
    });
  },

  'trabajadores.eliminarTodos'() {
    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    Trabajadores.remove({});
  },

  'trabajadores.eliminarSeleccionados'(seleccionados) {
    check(seleccionados, [String]);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {

      // Obtiene uid del trabajador a eliminar para obtener suss eventos de Entrada y Salida
      var uid = Trabajadores.findOne({_id:seleccionado}).uid;
      var events = Eventos.find({uid:uid,
          "$or": [{
              "tipo": "Entrada"
          }, {
              "tipo": "Salida"
          }]
      }).fetch().map((sel)=>{return sel._id});

      // Se eliminan trabajador, sus eventos de entrada y salida, y sus imagenes
      Meteor.call('eventos.eliminarSeleccionados',events);
      Meteor.call('imagenes.eliminar',uid);
      Trabajadores.remove({_id:seleccionado});
    });

  },

  'trabajadores.actualizar'(uid,rut,cargo,nombre,apellidop,apellidom) {

      check(rut, String);
      check(nombre, String);

      // Make sure the user is logged in before inserting a task
      if (! Meteor.userId()) {
        throw new Meteor.Error('not-authorized');
      }

      uid.map( (seleccionado) => {
        var trabajador = Trabajadores.findOne({uid:seleccionado});
        Trabajadores.update(trabajador._id,{ $set: { rut : rut, nombre : nombre, cargo : cargo, apellidop : apellidop, apellidom : apellidom } } );
      });

    },

});
