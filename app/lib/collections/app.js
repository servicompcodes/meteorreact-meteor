import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';


App = new Mongo.Collection('app');

if (Meteor.isServer) {
  Meteor.publish('app', function appPublicacion() {
    return App.find();
  });
}

Meteor.methods({
    'app.setTimeOut'(){

      Meteor.setInterval(function() {

        Meteor.call('arduinos.leer',function(err, res) {

          if(res===0){
            notification.open({
              message: 'No Hay Arduinos Conectados',
              placement:'bottomRight',
              icon: <Icon type="close-circle" style={{ color: 'red' }} />,
            });
          } else {
            notification.open({
              message: 'Hay ' + res + ' Arduinos Conectados',
              placement:'bottomRight',
              icon: <Icon type="check-circle" style={{ color: 'green' }} />,
            });
          }
        });

      },30000);
      

    }
});
