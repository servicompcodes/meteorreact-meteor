import { FilesCollection } from 'meteor/ostrio:files';

Images = new FilesCollection({
  collectionName: 'Images',
  allowClientCode: true, // Disallow remove files from Client
  storagePath: () => {
      return '/Users/3ms/Documents/ProyectosMeteor/tesis/uploads/';
  },
  onBeforeUpload(file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 10485760 && /png|jpg|jpeg/i.test(file.extension)) {
      return true;
    } else {
      return 'Please upload image, with size equal or less than 10MB';
    }
  }
});

/* Equal shortcut: */

if (Meteor.isServer) {
  Meteor.publish('files.images.all', function () {
    return Images.find().cursor;
  });
}

Meteor.methods({
  'imagenes.insertar'(image){

    Images.insert({
      file: image.file,
      isBase64: true, // <— Mandatory
      fileName: image.filename, // <— Mandatory
      type: 'image/png' // <— Mandatory
    });
  },

  'imagenes.eliminar'(uid){
    Images.findOne({'meta.uid': uid})?Images.findOne({'meta.uid': uid}).remove():'';
  },

  'RemoveFile'(uid) {
    Images.findOne({_id: uid}).remove((error) => {
        if(error){

        }
      }
    );
  }
});
