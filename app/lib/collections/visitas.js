import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Visitas = new Mongo.Collection('visitas');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('visitas', function visitasPublicacion() {
    return Visitas.find();
  });
}

Meteor.methods({

  'visitas.insertar'(uid,rut,nombre) {
    check(uid, String);
    check(rut, String);
    check(nombre, String);

    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Visitas.insert({
      uid,
      rut,
      nombre,
      fecha: new Date(),
      propietario: Meteor.userId(),
    });
  },
  'visitas.eliminarTodos'() {
    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    Visitas.remove({});
  },
  'visitas.eliminarSeleccionados'(seleccionados) {
    check(seleccionados, [String]);

    if (!Meteor.userId() || Meteor.user().username !== 'Administrador') {
      throw new Meteor.Error('not-authorized');
    }

    seleccionados.map( (seleccionado) => {

      // Obtiene uid del visita a eliminar para obtener suss eventos de Entrada y Salida
      var uid = Visitas.findOne({_id:seleccionado}).uid;
      var events = Eventos.find({uid:uid,
          "$or": [{
              "tipo": "Entrada"
          }, {
              "tipo": "Salida"
          }]
      }).fetch().map((sel)=>{return sel._id});

      // Se eliminan visita y sus eventos
      Meteor.call('eventos.eliminarSeleccionados',events);
      Meteor.call('imagenes.eliminar',uid);
      Visitas.remove({_id:seleccionado});
    });

  },
  'visitas.actualizar'(uid,rut,nombre) {

    check(rut, String);
    check(nombre, String);

    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    uid.map( (seleccionado) => {
      var visita = Visitas.findOne({uid:seleccionado});
      Visitas.update(visita._id,{ $set: { rut : rut, nombre : nombre } } );
    });

  },

});
